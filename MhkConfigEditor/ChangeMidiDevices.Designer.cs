﻿namespace MhkConfigEditor
{
	partial class ChangeMidiDevices
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.btnOk = new System.Windows.Forms.Button();
			this.cbMidiIn = new System.Windows.Forms.ComboBox();
			this.cbMidiOut = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.w_out = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.w_in = new System.Windows.Forms.Label();
			this.btnCancel = new System.Windows.Forms.Button();
			this.w1_tt = new System.Windows.Forms.ToolTip(this.components);
			this.npLoopInterval = new System.Windows.Forms.NumericUpDown();
			this.bLoopInterval = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.npLoopInterval)).BeginInit();
			this.SuspendLayout();
			// 
			// btnOk
			// 
			this.btnOk.Location = new System.Drawing.Point(70, 86);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(75, 23);
			this.btnOk.TabIndex = 0;
			this.btnOk.Text = "Ok";
			this.btnOk.UseVisualStyleBackColor = true;
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// cbMidiIn
			// 
			this.cbMidiIn.FormattingEnabled = true;
			this.cbMidiIn.Location = new System.Drawing.Point(86, 6);
			this.cbMidiIn.Name = "cbMidiIn";
			this.cbMidiIn.Size = new System.Drawing.Size(121, 21);
			this.cbMidiIn.TabIndex = 1;
			this.cbMidiIn.SelectedIndexChanged += new System.EventHandler(this.cbMidiIn_SelectedIndexChanged);
			// 
			// cbMidiOut
			// 
			this.cbMidiOut.FormattingEnabled = true;
			this.cbMidiOut.Location = new System.Drawing.Point(87, 33);
			this.cbMidiOut.Name = "cbMidiOut";
			this.cbMidiOut.Size = new System.Drawing.Size(121, 21);
			this.cbMidiOut.TabIndex = 2;
			this.cbMidiOut.SelectedIndexChanged += new System.EventHandler(this.cbMidiOut_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(37, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "Midi in";
			// 
			// w_out
			// 
			this.w_out.AutoSize = true;
			this.w_out.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.w_out.ForeColor = System.Drawing.Color.Red;
			this.w_out.Location = new System.Drawing.Point(214, 36);
			this.w_out.Name = "w_out";
			this.w_out.Size = new System.Drawing.Size(12, 15);
			this.w_out.TabIndex = 4;
			this.w_out.Text = "!";
			this.w1_tt.SetToolTip(this.w_out, "Warning: The already specified output device could not be found.\r\nIs it connected" +
        "?\r\n");
			this.w_out.Visible = false;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 36);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(44, 13);
			this.label3.TabIndex = 5;
			this.label3.Text = "Midi out";
			// 
			// w_in
			// 
			this.w_in.AutoSize = true;
			this.w_in.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.w_in.ForeColor = System.Drawing.Color.Red;
			this.w_in.Location = new System.Drawing.Point(214, 9);
			this.w_in.Name = "w_in";
			this.w_in.Size = new System.Drawing.Size(12, 15);
			this.w_in.TabIndex = 6;
			this.w_in.Text = "!";
			this.w1_tt.SetToolTip(this.w_in, "Warning: The already specified input device could not be found.\r\nIs it connected?" +
        "");
			this.w_in.Visible = false;
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(151, 86);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 8;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// w1_tt
			// 
			this.w1_tt.AutomaticDelay = 10;
			this.w1_tt.AutoPopDelay = 999999;
			this.w1_tt.InitialDelay = 10;
			this.w1_tt.ReshowDelay = 2;
			this.w1_tt.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
			// 
			// npLoopInterval
			// 
			this.npLoopInterval.Location = new System.Drawing.Point(87, 60);
			this.npLoopInterval.Name = "npLoopInterval";
			this.npLoopInterval.Size = new System.Drawing.Size(120, 20);
			this.npLoopInterval.TabIndex = 9;
			this.npLoopInterval.ValueChanged += new System.EventHandler(this.npLoopInterval_ValueChanged);
			// 
			// bLoopInterval
			// 
			this.bLoopInterval.AutoSize = true;
			this.bLoopInterval.Location = new System.Drawing.Point(12, 62);
			this.bLoopInterval.Name = "bLoopInterval";
			this.bLoopInterval.Size = new System.Drawing.Size(69, 13);
			this.bLoopInterval.TabIndex = 10;
			this.bLoopInterval.Text = "Loop Interval";
			// 
			// ChangeMidiDevices
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(255, 122);
			this.Controls.Add(this.bLoopInterval);
			this.Controls.Add(this.npLoopInterval);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.w_in);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.w_out);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cbMidiOut);
			this.Controls.Add(this.cbMidiIn);
			this.Controls.Add(this.btnOk);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ChangeMidiDevices";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "ChangeMidiDevices";
			this.Load += new System.EventHandler(this.ChangeMidiDevices_Load);
			((System.ComponentModel.ISupportInitialize)(this.npLoopInterval)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.ComboBox cbMidiIn;
		private System.Windows.Forms.ComboBox cbMidiOut;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label w_out;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label w_in;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.ToolTip w1_tt;
		private System.Windows.Forms.NumericUpDown npLoopInterval;
		private System.Windows.Forms.Label bLoopInterval;
	}
}