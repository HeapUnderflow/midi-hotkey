﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MhkConfigEditor
{
	public partial class ChangeMidiDevices : Form
	{
		private List<string> midi_in = new List<string>();
		private List<string> midi_out = new List<string>();

		public  bool Success { get; private set; } = false;
		public ulong LoopInterval { get; private set; } = 0;
		public string SelectedMidiIn { get; private set; }
		public string SelectedMidiOut { get; private set; }

		public ChangeMidiDevices(string i, string o, ulong lp)
		{
			InitializeComponent();
			SelectedMidiIn = i;
			SelectedMidiOut = o;
			LoopInterval = lp;
		}

		private void ChangeMidiDevices_Load(object sender, EventArgs e)
		{
			var lm = Utils.GetMidiDevices();

			midi_in.AddRange(lm.Inputs.ToList());
			midi_out.AddRange(lm.Outputs.ToList());

			cbMidiIn.Items.AddRange(midi_in.ToArray());
			cbMidiOut.Items.AddRange(midi_out.ToArray());

			cbMidiIn.DropDownWidth = Utils.DropDownWidth(cbMidiIn);
			cbMidiOut.DropDownWidth = Utils.DropDownWidth(cbMidiOut);

			// "Alerts"
			if (!midi_in.Any((v) => v == SelectedMidiIn))
			{
				int val;
				if (int.TryParse(SelectedMidiIn, out val))
				{
					SelectedMidiIn = midi_in[val];
				}
				else
				{
					w_in.Visible = true;
				}
			}
			else
			{
				cbMidiIn.SelectedItem = SelectedMidiIn;
			}

			if (!midi_out.Any((v) => v == SelectedMidiOut))
			{
				int val;
				if (int.TryParse(SelectedMidiOut, out val))
				{
					SelectedMidiOut = midi_out[val];
				}
				else
				{
					w_out.Visible = true;
				}
			}
			else
			{
				cbMidiOut.SelectedItem = SelectedMidiOut;
			}

			npLoopInterval.Value = LoopInterval;
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			Success = true;
			Close();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			Success = false;
			Close();
		}

		private void cbMidiIn_SelectedIndexChanged(object sender, EventArgs e)
		{
			SelectedMidiIn = midi_in[cbMidiIn.SelectedIndex];
			w_in.Visible = false;
		}

		private void cbMidiOut_SelectedIndexChanged(object sender, EventArgs e)
		{
			SelectedMidiOut = midi_out[cbMidiOut.SelectedIndex];
			w_out.Visible = false;
		}

		private void npLoopInterval_ValueChanged(object sender, EventArgs e)
		{
			LoopInterval = (ulong)npLoopInterval.Value;
		}
	}
}
