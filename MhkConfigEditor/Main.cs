﻿using MhkConfigEditor.Models;
using System;
using System.Windows.Forms;

namespace MhkConfigEditor
{
	public partial class Main : Form
	{
		ConfigFile config;
		private readonly string _path;
		public Main(string path = "config.json")
		{
			_path = path;
			InitializeComponent();
			data_view_note.Columns[data_view_note.Columns.Count - 1].Width = -2;
			data_view_controller.Columns[data_view_controller.Columns.Count - 1].Width = -2;
		}

		private void Main_ResizeEnd(object sender, EventArgs e)
		{
			// Resize last column of listview
			data_view_note.Columns[data_view_note.Columns.Count - 1].Width = -2;
			data_view_controller.Columns[data_view_controller.Columns.Count - 1].Width = -2;
		}

		private void Main_Load(object sender, EventArgs e)
		{
			config = ConfigFile.Load(_path);
			lMidiIn.Text = "Midi In: " + config.MidiIn;
			lMidiOut.Text = "Midi Out: " + (config.MidiOut ?? "[nowhere]");
			lLoopInterval.Text = "Loop Interval: " + config.LoopInterval;
			foreach (NoteMapping nmap in config.Notes)
			{
				data_view_note.Items.Add(Utils.NoteToListviewItem(nmap));
			}

			foreach (ControllerMapping cmap in config.Controllers)
			{

				data_view_controller.Items.Add(Utils.ControllerToListviewItem(cmap));
			}

			Text = "Midi Hotkey Configurator [" + _path + "]";
		}

		private void btnAddNote_Click(object sender, EventArgs e)
		{
			ChangeNote nt = new ChangeNote();
			nt.ShowDialog();
			if (nt.Success)
			{
				config.Notes.Add(nt.Result);
				data_view_note.Items.Add(Utils.NoteToListviewItem(nt.Result));
			}
		}

		private void btnAddController_Click(object sender, EventArgs e)
		{
			ChangeController ct = new ChangeController();
			ct.ShowDialog();
			if (ct.Success)
			{
				config.Controllers.Add(ct.Result);
				data_view_controller.Items.Add(Utils.ControllerToListviewItem(ct.Result));
			}
		}

		private void btnChangeMidiDev_Click(object sender, EventArgs e)
		{
			ChangeMidiDevices cmd = new ChangeMidiDevices(config.MidiIn, config.MidiOut, config.LoopInterval);
			cmd.ShowDialog();
			if (cmd.Success)
			{
				config.MidiIn = cmd.SelectedMidiIn;
				config.MidiOut = cmd.SelectedMidiOut;
				config.LoopInterval = cmd.LoopInterval;

				lMidiIn.Text = "Midi In: " + config.MidiIn;
				lMidiOut.Text = "Midi Out: " + (config.MidiOut ?? "[nowhere]");
				lLoopInterval.Text = "Loop Interval: " + config.LoopInterval;
			}
		}

		private void btnSaveAll_Click(object sender, EventArgs e)
		{
			// TODO: Do Saving
			config.Store(_path);
		}

		private void data_view_note_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			int idx = data_view_note.SelectedIndices[0];
			ChangeNote nt = new ChangeNote(config.Notes[idx]);
			nt.ShowDialog();
			if (nt.Success && nt.DeleteNote)
			{
				config.Notes.RemoveAt(idx);
				data_view_note.Items.RemoveAt(idx);
			}
			else if (nt.Success && !nt.DeleteNote)
			{
				config.Notes[idx] = nt.Result;
				data_view_note.Items[idx] = Utils.NoteToListviewItem(nt.Result);
			}
		}

		private void data_view_controller_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			int idx = data_view_controller.SelectedIndices[0];
			ChangeController ct = new ChangeController(config.Controllers[idx]);
			ct.ShowDialog();

			if (ct.Success && ct.Delete)
			{
				config.Controllers.RemoveAt(idx);
				data_view_controller.Items.RemoveAt(idx);
			}
			else if (ct.Success && !ct.Delete)
			{
				config.Controllers[idx] = ct.Result;
				data_view_controller.Items[idx] = Utils.ControllerToListviewItem(ct.Result);
			}
		}

		private void data_view_note_MouseClick(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				if (data_view_note.SelectedItems.Count < 1 || data_view_note.Items.Count < 1) { data_view_note.ContextMenu = EmptyContextMenu(); return; }
				var idx = data_view_note.SelectedIndices[0];

				ContextMenu cm = new ContextMenu();

				MenuItem mn_up = new MenuItem
				{
					Text = "Move Up",
					Enabled = idx > 0
				};
				mn_up.Click += (snd, _e) =>
				{
					if (data_view_note.Items.Count < 1)
					{
						data_view_note.ContextMenu = EmptyContextMenu();
						return;
					}
					config.Notes.Swap(idx, idx - 1);
					data_view_note.Items[idx] = Utils.NoteToListviewItem(config.Notes[idx]);
					data_view_note.Items[idx - 1] = Utils.NoteToListviewItem(config.Notes[idx - 1]);
				};

				MenuItem mn_down = new MenuItem
				{
					Text = "Move Down",
					Enabled = idx < config.Notes.Count - 1
				};
				mn_down.Click += (snd, _e) =>
				{
					if (data_view_note.Items.Count < 1)
					{
						data_view_note.ContextMenu = EmptyContextMenu();
						return;
					}
					config.Notes.Swap(idx, idx + 1);
					data_view_note.Items[idx] = Utils.NoteToListviewItem(config.Notes[idx]);
					data_view_note.Items[idx + 1] = Utils.NoteToListviewItem(config.Notes[idx + 1]);
				};

				cm.MenuItems.AddRange(new[] { mn_up, mn_down });

				data_view_note.ContextMenu = cm;
			}
		}

		private void data_view_controller_MouseClick(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				if (data_view_controller.SelectedItems.Count < 1
					|| data_view_controller.Items.Count < 1)
				{
					data_view_controller.ContextMenu = EmptyContextMenu();
					return;
				}
				var idx = data_view_controller.SelectedIndices[0];

				ContextMenu cm = new ContextMenu();

				MenuItem mn_up = new MenuItem();
				mn_up.Text = "Move Up";
				mn_up.Enabled = idx > 0;
				mn_up.Click += (snd, _e) =>
				{
					if (data_view_controller.Items.Count < 1)
					{
						data_view_controller.ContextMenu = EmptyContextMenu();
						return;
					}
					config.Controllers.Swap(idx, idx - 1);
					data_view_controller.Items[idx] = Utils.ControllerToListviewItem(config.Controllers[idx]);
					data_view_controller.Items[idx - 1] = Utils.ControllerToListviewItem(config.Controllers[idx - 1]);
				};

				MenuItem mn_down = new MenuItem();
				mn_down.Text = "Move Down";
				mn_down.Enabled = idx < config.Controllers.Count - 1;
				mn_down.Click += (snd, _e) =>
				{
					if (data_view_controller.Items.Count < 1)
					{
						data_view_controller.ContextMenu = EmptyContextMenu();
						return;
					}
					config.Controllers.Swap(idx, idx + 1);
					data_view_controller.Items[idx] = Utils.ControllerToListviewItem(config.Controllers[idx]);
					data_view_controller.Items[idx + 1] = Utils.ControllerToListviewItem(config.Controllers[idx + 1]);
				};

				cm.MenuItems.AddRange(new[] { mn_up, mn_down });

				data_view_controller.ContextMenu = cm;
			}
		}

		private static ContextMenu EmptyContextMenu()
		{
			ContextMenu m = new ContextMenu();
			m.MenuItems.Add("Move Up");
			m.MenuItems.Add("Move Down");
			m.MenuItems[0].Enabled = false;
			m.MenuItems[1].Enabled = false;
			return m;
		}
	}
}
