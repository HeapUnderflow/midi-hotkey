﻿namespace MhkConfigEditor
{
	partial class ChangeController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.cbController = new System.Windows.Forms.ComboBox();
			this.cbKey = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.questionmarkHelp = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.chkEnableAra = new System.Windows.Forms.CheckBox();
			this.araAfter = new System.Windows.Forms.NumericUpDown();
			this.araTooltipHelp = new System.Windows.Forms.ToolTip(this.components);
			this.npMidPoint = new System.Windows.Forms.NumericUpDown();
			this.npDeadZone = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.btnOk = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnRemove = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.araAfter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.npMidPoint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.npDeadZone)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(51, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Controller";
			// 
			// cbController
			// 
			this.cbController.FormattingEnabled = true;
			this.cbController.Location = new System.Drawing.Point(83, 6);
			this.cbController.Name = "cbController";
			this.cbController.Size = new System.Drawing.Size(121, 21);
			this.cbController.TabIndex = 2;
			// 
			// cbKey
			// 
			this.cbKey.FormattingEnabled = true;
			this.cbKey.Location = new System.Drawing.Point(83, 33);
			this.cbKey.Name = "cbKey";
			this.cbKey.Size = new System.Drawing.Size(121, 21);
			this.cbKey.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 36);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(25, 13);
			this.label2.TabIndex = 4;
			this.label2.Text = "Key";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.questionmarkHelp);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.chkEnableAra);
			this.groupBox1.Controls.Add(this.araAfter);
			this.groupBox1.Location = new System.Drawing.Point(210, 4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(165, 74);
			this.groupBox1.TabIndex = 7;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Auto Return";
			// 
			// questionmarkHelp
			// 
			this.questionmarkHelp.AutoSize = true;
			this.questionmarkHelp.Location = new System.Drawing.Point(139, 16);
			this.questionmarkHelp.Name = "questionmarkHelp";
			this.questionmarkHelp.Size = new System.Drawing.Size(13, 13);
			this.questionmarkHelp.TabIndex = 10;
			this.questionmarkHelp.Text = "?";
			this.araTooltipHelp.SetToolTip(this.questionmarkHelp, "The time in milliseconds before the Key is autmatically unpressed\r\n(even if the c" +
        "orresponding controller has not yet been raised)");
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(139, 44);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(20, 13);
			this.label4.TabIndex = 9;
			this.label4.Text = "ms";
			// 
			// chkEnableAra
			// 
			this.chkEnableAra.AutoSize = true;
			this.chkEnableAra.Location = new System.Drawing.Point(15, 19);
			this.chkEnableAra.Name = "chkEnableAra";
			this.chkEnableAra.Size = new System.Drawing.Size(59, 17);
			this.chkEnableAra.TabIndex = 4;
			this.chkEnableAra.Text = "Enable";
			this.chkEnableAra.UseVisualStyleBackColor = true;
			this.chkEnableAra.CheckedChanged += new System.EventHandler(this.chkEnableAra_CheckedChanged);
			// 
			// araAfter
			// 
			this.araAfter.Enabled = false;
			this.araAfter.Location = new System.Drawing.Point(15, 42);
			this.araAfter.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
			this.araAfter.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.araAfter.Name = "araAfter";
			this.araAfter.Size = new System.Drawing.Size(120, 20);
			this.araAfter.TabIndex = 5;
			this.araAfter.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			// 
			// araTooltipHelp
			// 
			this.araTooltipHelp.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
			this.araTooltipHelp.ToolTipTitle = "Auto Return";
			// 
			// npMidPoint
			// 
			this.npMidPoint.Location = new System.Drawing.Point(83, 58);
			this.npMidPoint.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
			this.npMidPoint.Name = "npMidPoint";
			this.npMidPoint.Size = new System.Drawing.Size(120, 20);
			this.npMidPoint.TabIndex = 8;
			// 
			// npDeadZone
			// 
			this.npDeadZone.Location = new System.Drawing.Point(84, 84);
			this.npDeadZone.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
			this.npDeadZone.Name = "npDeadZone";
			this.npDeadZone.Size = new System.Drawing.Size(120, 20);
			this.npDeadZone.TabIndex = 9;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(12, 60);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(65, 13);
			this.label5.TabIndex = 10;
			this.label5.Text = "Middle Point";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(12, 86);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(61, 13);
			this.label6.TabIndex = 11;
			this.label6.Text = "Dead Zone";
			// 
			// btnOk
			// 
			this.btnOk.Location = new System.Drawing.Point(13, 110);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(75, 23);
			this.btnOk.TabIndex = 12;
			this.btnOk.Text = "Ok";
			this.btnOk.UseVisualStyleBackColor = true;
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(94, 110);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 13;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnRemove
			// 
			this.btnRemove.Location = new System.Drawing.Point(301, 110);
			this.btnRemove.Name = "btnRemove";
			this.btnRemove.Size = new System.Drawing.Size(75, 23);
			this.btnRemove.TabIndex = 14;
			this.btnRemove.Text = "Remove";
			this.btnRemove.UseVisualStyleBackColor = true;
			this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
			// 
			// ChangeController
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(384, 141);
			this.Controls.Add(this.btnRemove);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.npDeadZone);
			this.Controls.Add(this.npMidPoint);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.cbKey);
			this.Controls.Add(this.cbController);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Name = "ChangeController";
			this.Text = "ChangeController";
			this.Load += new System.EventHandler(this.ChangeController_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.araAfter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.npMidPoint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.npDeadZone)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cbController;
		private System.Windows.Forms.ComboBox cbKey;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label questionmarkHelp;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.CheckBox chkEnableAra;
		private System.Windows.Forms.NumericUpDown araAfter;
		private System.Windows.Forms.ToolTip araTooltipHelp;
		private System.Windows.Forms.NumericUpDown npMidPoint;
		private System.Windows.Forms.NumericUpDown npDeadZone;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnRemove;
	}
}