﻿using MhkConfigEditor.Models;
using System;
using System.Windows.Forms;

namespace MhkConfigEditor
{
	public partial class ChangeNote : Form
	{
		public bool Success { get; private set; } = false;
		public bool DeleteNote { get; private set; } = false;

		public NoteMapping Result { get; }

		private readonly bool _new;

		public ChangeNote(NoteMapping note = null)
		{
			Result = note ?? new NoteMapping("C5", "C");
			_new = note == null;
			InitializeComponent();
			araTooltipHelp.SetToolTip(questionmarkHelp, 
				"Auto Return is the Delay after which the Key is automatically raised, " +
				"even if the corresponding midi note is still on. " +
				"Time is in Milliseconds."
			);
		}

		private void ChangeNote_Load(object sender, EventArgs e)
		{
			cboxNote.Items.AddRange(Statics.Notes);
			cboxNote.SelectedItem = Result.Note;

			cboxKey.Items.AddRange(Statics.Keys);
			cboxKey.SelectedItem = Result.KeyCode;

			channel.Value = Result.Channel;
			minVel.Value = Result.MinimumVelocity;
			maxVel.Value = Result.MaximumVelocity;

			chkEnableAra.Checked = Result.AutoReturnAfter != null;
			araAfter.Enabled = Result.AutoReturnAfter != null;
			araAfter.Value = Result.AutoReturnAfter ?? 100;

			if (_new) { btnRemoveNote.Enabled = false; }
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			Success = true;

			Result.Note = (string)cboxNote.SelectedItem;
			Result.KeyCode = (string)cboxKey.SelectedItem;
			Result.AutoReturnAfter = chkEnableAra.Checked ? (int?)araAfter.Value : null;
			Result.Channel = (byte)channel.Value;
			Result.MinimumVelocity = (byte)minVel.Value;
			Result.MaximumVelocity = (byte)maxVel.Value;

			Close();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			Success = false;
			Close();
		}

		private void chkEnableAra_CheckedChanged(object sender, EventArgs e)
		{
			araAfter.Enabled = chkEnableAra.Checked;
		}

		private void btnRemoveNote_Click(object sender, EventArgs e)
		{
			DeleteNote = true;
			Success = true;
			Close();
		}
	}
}
