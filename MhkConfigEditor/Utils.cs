﻿using MhkConfigEditor.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Windows.Forms;

namespace MhkConfigEditor
{
	public static class Utils
	{
		public static ListViewItem NoteToListviewItem(NoteMapping note)
		{
			var ara = note.AutoReturnAfter == null ? "Never" : note.AutoReturnAfter.ToString();
			return new ListViewItem(
				new string[]
				{
					note.Note,
					note.KeyCode,
					$"Channel: {note.Channel}, " +
					$"Minimum Velocity: {note.MinimumVelocity}, " +
					$"Maximum Velocity: {note.MaximumVelocity}, " +
					$"Auto Return After: {ara}"
				}
			);
		}

		public static ListViewItem ControllerToListviewItem(ControllerMapping ctrl)
		{
			var ara = ctrl.AutoReturnAfter == null ? "Never" : ctrl.AutoReturnAfter.ToString();
			return new ListViewItem(
				new string[]
				{
					ctrl.Controller,
					ctrl.KeyCode,
					$"MidPoint: {ctrl.MidPoint}, " +
					$"DeadZone: {ctrl.DeadZone}, " +
					$"Auto Return After: {ara}"
				}
			);
		}

		public static int DropDownWidth(ComboBox myCombo)
		{
			int maxWidth = 0, temp = 0;
			foreach (var obj in myCombo.Items)
			{
				temp = TextRenderer.MeasureText(obj.ToString(), myCombo.Font).Width;
				if (temp > maxWidth)
				{
					maxWidth = temp;
				}
			}
			return maxWidth;
		}

		public static ListMidi GetMidiDevices()
		{
			Process p = new Process();
			p.StartInfo.FileName = "midi_hk.exe";
			p.StartInfo.Arguments = "--list-midi json";
			p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
			p.StartInfo.CreateNoWindow = true;
			p.StartInfo.UseShellExecute = false;
			p.StartInfo.RedirectStandardOutput = true;
			p.Start();
			var data = p.StandardOutput.ReadToEnd();

			JObject result = (JObject)JToken.Parse(data);
			Console.WriteLine(result);

			if ("ok" != ((string)result["status"]))
			{
				return null;
			}

			var i = result["data"]["input"].ToArray().Select((v) => (string)v).ToArray();
			var o = result["data"]["output"].ToArray().Select((v) => (string)v).ToArray();
			return new ListMidi(i, o);
		}

		public static void Swap(this IList list, int firstIndex, int secondIndex)
		{
			Contract.Requires(list != null);
			Contract.Requires(firstIndex >= 0 && firstIndex < list.Count);
			Contract.Requires(secondIndex >= 0 && secondIndex < list.Count);
			if (firstIndex == secondIndex)
			{
				return;
			}
			object temp = list[firstIndex];
			list[firstIndex] = list[secondIndex];
			list[secondIndex] = temp;
		}
	}
}
