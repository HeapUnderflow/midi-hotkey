﻿using MhkConfigEditor.Models;
using System;
using System.Windows.Forms;

namespace MhkConfigEditor
{
	public partial class ChangeController : Form
	{
		public ControllerMapping Result { get; }
		public bool Success { get; private set; } = false;
		public bool Delete { get; private set; } = false;

		private readonly bool _new;

		public ChangeController(ControllerMapping mp = null)
		{
			Result = mp ?? new ControllerMapping("Foot_Controller", "SPACE");
			_new = mp == null;
			InitializeComponent();
		}

		private void ChangeController_Load(object sender, EventArgs e)
		{
			cbController.Items.AddRange(Statics.Controllers);
			cbController.SelectedItem = Result.Controller;

			cbKey.Items.AddRange(Statics.Keys);
			cbKey.SelectedItem = Result.KeyCode;

			npMidPoint.Value = Result.MidPoint;
			npDeadZone.Value = Result.DeadZone;

			chkEnableAra.Checked = Result.AutoReturnAfter != null;
			araAfter.Enabled = Result.AutoReturnAfter != null;
			araAfter.Value = Result.AutoReturnAfter ?? 100;

			if (_new) { btnRemove.Enabled = false; }
		}

		private void chkEnableAra_CheckedChanged(object sender, EventArgs e)
		{
			araAfter.Enabled = chkEnableAra.Checked;
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			Success = true;

			Result.Controller = (string)cbController.SelectedItem;
			Result.KeyCode = (string)cbKey.SelectedItem;

			Result.MidPoint = (byte)npMidPoint.Value;
			Result.DeadZone = (byte)npDeadZone.Value;

			Result.AutoReturnAfter = chkEnableAra.Checked ? (int?)araAfter.Value : null;

			Close();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			Success = false;
			Close();
		}

		private void btnRemove_Click(object sender, EventArgs e)
		{
			Success = true;
			Delete = true;
			Close();
		}
	}
}
