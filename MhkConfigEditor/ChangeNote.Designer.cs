﻿namespace MhkConfigEditor
{
	partial class ChangeNote
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.cboxNote = new System.Windows.Forms.ComboBox();
			this.cboxKey = new System.Windows.Forms.ComboBox();
			this.btnOk = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.chkEnableAra = new System.Windows.Forms.CheckBox();
			this.araAfter = new System.Windows.Forms.NumericUpDown();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.questionmarkHelp = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.araTooltipHelp = new System.Windows.Forms.ToolTip(this.components);
			this.label4 = new System.Windows.Forms.Label();
			this.channel = new System.Windows.Forms.NumericUpDown();
			this.btnRemoveNote = new System.Windows.Forms.Button();
			this.minVel = new System.Windows.Forms.NumericUpDown();
			this.maxVel = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.araAfter)).BeginInit();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.channel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.minVel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.maxVel)).BeginInit();
			this.SuspendLayout();
			// 
			// cboxNote
			// 
			this.cboxNote.FormattingEnabled = true;
			this.cboxNote.Location = new System.Drawing.Point(38, 15);
			this.cboxNote.Name = "cboxNote";
			this.cboxNote.Size = new System.Drawing.Size(121, 21);
			this.cboxNote.TabIndex = 0;
			// 
			// cboxKey
			// 
			this.cboxKey.FormattingEnabled = true;
			this.cboxKey.Location = new System.Drawing.Point(38, 39);
			this.cboxKey.Name = "cboxKey";
			this.cboxKey.Size = new System.Drawing.Size(121, 21);
			this.cboxKey.TabIndex = 1;
			// 
			// btnOk
			// 
			this.btnOk.Location = new System.Drawing.Point(10, 142);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(68, 23);
			this.btnOk.TabIndex = 2;
			this.btnOk.Text = "Ok";
			this.btnOk.UseVisualStyleBackColor = true;
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(84, 142);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// chkEnableAra
			// 
			this.chkEnableAra.AutoSize = true;
			this.chkEnableAra.Location = new System.Drawing.Point(15, 19);
			this.chkEnableAra.Name = "chkEnableAra";
			this.chkEnableAra.Size = new System.Drawing.Size(59, 17);
			this.chkEnableAra.TabIndex = 4;
			this.chkEnableAra.Text = "Enable";
			this.chkEnableAra.UseVisualStyleBackColor = true;
			this.chkEnableAra.CheckedChanged += new System.EventHandler(this.chkEnableAra_CheckedChanged);
			// 
			// araAfter
			// 
			this.araAfter.Enabled = false;
			this.araAfter.Location = new System.Drawing.Point(15, 42);
			this.araAfter.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
			this.araAfter.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.araAfter.Name = "araAfter";
			this.araAfter.Size = new System.Drawing.Size(120, 20);
			this.araAfter.TabIndex = 5;
			this.araAfter.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.questionmarkHelp);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.chkEnableAra);
			this.groupBox1.Controls.Add(this.araAfter);
			this.groupBox1.Location = new System.Drawing.Point(167, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(165, 74);
			this.groupBox1.TabIndex = 6;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Auto Return";
			// 
			// questionmarkHelp
			// 
			this.questionmarkHelp.AutoSize = true;
			this.questionmarkHelp.Location = new System.Drawing.Point(139, 16);
			this.questionmarkHelp.Name = "questionmarkHelp";
			this.questionmarkHelp.Size = new System.Drawing.Size(13, 13);
			this.questionmarkHelp.TabIndex = 10;
			this.questionmarkHelp.Text = "?";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(139, 44);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(20, 13);
			this.label3.TabIndex = 9;
			this.label3.Text = "ms";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(7, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(30, 13);
			this.label1.TabIndex = 7;
			this.label1.Text = "Note";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(7, 42);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(25, 13);
			this.label2.TabIndex = 8;
			this.label2.Text = "Key";
			// 
			// araTooltipHelp
			// 
			this.araTooltipHelp.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
			this.araTooltipHelp.ToolTipTitle = "Auto Return";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(7, 68);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(26, 13);
			this.label4.TabIndex = 10;
			this.label4.Text = "Chn";
			// 
			// channel
			// 
			this.channel.Location = new System.Drawing.Point(38, 66);
			this.channel.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
			this.channel.Name = "channel";
			this.channel.Size = new System.Drawing.Size(123, 20);
			this.channel.TabIndex = 11;
			// 
			// btnRemoveNote
			// 
			this.btnRemoveNote.Location = new System.Drawing.Point(257, 142);
			this.btnRemoveNote.Name = "btnRemoveNote";
			this.btnRemoveNote.Size = new System.Drawing.Size(75, 23);
			this.btnRemoveNote.TabIndex = 12;
			this.btnRemoveNote.Text = "Remove";
			this.btnRemoveNote.UseVisualStyleBackColor = true;
			this.btnRemoveNote.Click += new System.EventHandler(this.btnRemoveNote_Click);
			// 
			// minVel
			// 
			this.minVel.Location = new System.Drawing.Point(76, 92);
			this.minVel.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
			this.minVel.Name = "minVel";
			this.minVel.Size = new System.Drawing.Size(123, 20);
			this.minVel.TabIndex = 13;
			// 
			// maxVel
			// 
			this.maxVel.Location = new System.Drawing.Point(76, 116);
			this.maxVel.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
			this.maxVel.Name = "maxVel";
			this.maxVel.Size = new System.Drawing.Size(123, 20);
			this.maxVel.TabIndex = 14;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 94);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(64, 13);
			this.label5.TabIndex = 15;
			this.label5.Text = "Min-Velocity";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(6, 118);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(67, 13);
			this.label6.TabIndex = 16;
			this.label6.Text = "Max-Velocity";
			// 
			// ChangeNote
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(344, 174);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.maxVel);
			this.Controls.Add(this.minVel);
			this.Controls.Add(this.btnRemoveNote);
			this.Controls.Add(this.channel);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.cboxKey);
			this.Controls.Add(this.cboxNote);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ChangeNote";
			this.Text = "ChangeNote";
			this.Load += new System.EventHandler(this.ChangeNote_Load);
			((System.ComponentModel.ISupportInitialize)(this.araAfter)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.channel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.minVel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.maxVel)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox cboxNote;
		private System.Windows.Forms.ComboBox cboxKey;
		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.CheckBox chkEnableAra;
		private System.Windows.Forms.NumericUpDown araAfter;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label questionmarkHelp;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ToolTip araTooltipHelp;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown channel;
		private System.Windows.Forms.Button btnRemoveNote;
		private System.Windows.Forms.NumericUpDown minVel;
		private System.Windows.Forms.NumericUpDown maxVel;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
	}
}