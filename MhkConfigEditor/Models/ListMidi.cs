﻿namespace MhkConfigEditor.Models
{
	public class ListMidi
	{
		public readonly string[] Inputs;
		public readonly string[] Outputs;

		public ListMidi(string[] i, string[] o)
		{
			Inputs = i;
			Outputs = o;
		}
	}
}
