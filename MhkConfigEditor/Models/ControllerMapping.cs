﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MhkConfigEditor.Models
{
	public class ControllerMapping
	{
		public string Controller;
		public string KeyCode;
		public byte DeadZone;
		public byte MidPoint;
		public int? AutoReturnAfter;

		public ControllerMapping(string ctrl, string kc, byte dz = 0, byte mp = 64, int? ara = null)
		{
			Controller = ctrl;
			KeyCode = kc;
			DeadZone = dz;
			MidPoint = mp;
			AutoReturnAfter = ara;
		}
	}
}

/*
type Controllers = Vec<Controller>;
#[derive(Deserialize, Serialize, Debug, Clone, Eq, PartialEq)]
pub struct Controller {
	pub controller: MidiController,
	pub keycode:    VirtualKey,
	pub deadzone:   u8,
	pub midpoint:   u8,
	pub ara:        Option<u64>,
}
*/
