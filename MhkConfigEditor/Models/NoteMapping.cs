﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MhkConfigEditor.Models
{
	public class NoteMapping
	{
		public string Note;
		public string KeyCode;
		public byte Channel;
		public byte MinimumVelocity;
		public byte MaximumVelocity;
		public int? AutoReturnAfter;

		public NoteMapping(string nt, string kc, byte ch = 0, byte mnv = 0, byte mxv = 127, int? ara = null)
		{
			Note = nt;
			KeyCode = kc;
			Channel = ch;
			MinimumVelocity = mnv;
			MaximumVelocity = mxv;
			AutoReturnAfter = ara;
		}
	}
}

/*
pub type Notes = Vec<Note>;
#[derive(Deserialize, Serialize, Debug, Clone, Eq, PartialEq)]
pub struct Note {
	pub note:    MidiNote,
	pub keycode: VirtualKey,
	// pub event_type: Option<bool>,
	pub channel: u8,
	pub min_vel: u8,
	pub max_vel: u8,
	pub ara:     Option<u64>,
}
*/
