﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;

namespace MhkConfigEditor.Models
{
	public class ConfigFile
	{
		public string MidiIn;
		public string MidiOut;
		public ulong LoopInterval;
		public List<NoteMapping> Notes;
		public List<ControllerMapping> Controllers;

		public class JsonParseException : Exception { }

		private ConfigFile(string mi, string mo, ulong lp, List<NoteMapping> nm, List<ControllerMapping> ct) {
			MidiIn = mi;
			MidiOut = mo;
			LoopInterval = lp;
			Notes = nm;
			Controllers = ct;
  }

		public static ConfigFile Load(string path)
		{
			if (!File.Exists(path))
			{
				Process p = new Process();
				p.StartInfo.FileName = ".\\midi_hk.exe";
				p.StartInfo.Arguments = $"--dump-empty {path}";
				p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
				p.StartInfo.CreateNoWindow = true;
				p.Start();
				p.WaitForExit();
			}

			using (StreamReader sr = new StreamReader(File.OpenRead(path)))
			{
				JObject data = (JObject)JToken.ReadFrom(new JsonTextReader(sr));

				string min = (string)data["midi_in"];
				string mout = (string)data["midi_out"];
				ulong lp = (ulong)data["loop_interval"];

				List<NoteMapping> nm = new List<NoteMapping>();
				foreach (JObject val in data["notes"])
				{
					var nv = new NoteMapping(
						(string)val["note"],
						(string)val["keycode"],
						(byte)val["channel"],
						(byte)val["min_vel"],
						(byte)val["max_vel"],
						(int?)val["ara"]);
					nm.Add(nv);
				}
				List<ControllerMapping> ct = new List<ControllerMapping>();
				foreach(JObject val in data["controllers"])
				{
					var nv = new ControllerMapping(
						(string)val["controller"],
						(string)val["keycode"],
						(byte)val["deadzone"],
						(byte)val["midpoint"],
						(int?)val["ara"]);
					ct.Add(nv);
				}

				return new ConfigFile(min, mout, lp, nm, ct);
			}
		}

		public void Store(string path)
		{
			Func<string, string, JProperty> maybe_number = (string name, string val) =>
			{
				int v = -1;
				if (int.TryParse(val, out v))
				{
					return new JProperty(name, v);
				}
				else
				{
					return new JProperty(name, val);
				}
			};

			using (StreamWriter sw = new StreamWriter(File.Create($"{path}.tmp")))
			{
				JObject data = new JObject(
					maybe_number("midi_in", MidiIn),
					maybe_number("midi_out", MidiOut),
					new JProperty("loop_interval", LoopInterval),
					new JProperty("notes", new JArray(
						from note in Notes
						select new JObject(
							maybe_number("note", note.Note),
							maybe_number("keycode", note.KeyCode),
							new JProperty("channel", note.Channel),
							new JProperty("min_vel", note.MinimumVelocity),
							new JProperty("max_vel", note.MaximumVelocity),
							new JProperty("ara", note.AutoReturnAfter)))),
					new JProperty("controllers", new JArray(
						from ctrl in Controllers
						select new JObject(
							maybe_number("controller", ctrl.Controller),
							maybe_number("keycode", ctrl.KeyCode),
							new JProperty("deadzone", ctrl.DeadZone),
							new JProperty("midpoint", ctrl.MidPoint),
							new JProperty("ara", ctrl.AutoReturnAfter)))));
				data.WriteTo(new JsonTextWriter(sw));
				sw.Flush();
			}

			if (File.Exists(path))
			{
				File.Delete(path);
			}
			File.Move($"{path}.tmp", path);
		}
	}
}
