﻿namespace MhkConfigEditor
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.data_view_note = new System.Windows.Forms.ListView();
			this.chead_nt = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.chead_key = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.chead_spdata = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.data_view_controller = new System.Windows.Forms.ListView();
			this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lMidiIn = new System.Windows.Forms.Label();
			this.btnAddNote = new System.Windows.Forms.Button();
			this.lMidiOut = new System.Windows.Forms.Label();
			this.btnChangeMidiDev = new System.Windows.Forms.Button();
			this.btnAddController = new System.Windows.Forms.Button();
			this.btnSaveAll = new System.Windows.Forms.Button();
			this.lLoopInterval = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// data_view_note
			// 
			this.data_view_note.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.data_view_note.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chead_nt,
            this.chead_key,
            this.chead_spdata});
			this.data_view_note.FullRowSelect = true;
			this.data_view_note.GridLines = true;
			this.data_view_note.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.data_view_note.HideSelection = false;
			this.data_view_note.Location = new System.Drawing.Point(12, 25);
			this.data_view_note.MultiSelect = false;
			this.data_view_note.Name = "data_view_note";
			this.data_view_note.Size = new System.Drawing.Size(648, 300);
			this.data_view_note.TabIndex = 1;
			this.data_view_note.UseCompatibleStateImageBehavior = false;
			this.data_view_note.View = System.Windows.Forms.View.Details;
			this.data_view_note.MouseClick += new System.Windows.Forms.MouseEventHandler(this.data_view_note_MouseClick);
			this.data_view_note.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.data_view_note_MouseDoubleClick);
			// 
			// chead_nt
			// 
			this.chead_nt.Text = "Note";
			this.chead_nt.Width = 116;
			// 
			// chead_key
			// 
			this.chead_key.Text = "Key";
			this.chead_key.Width = 95;
			// 
			// chead_spdata
			// 
			this.chead_spdata.Text = "Data";
			this.chead_spdata.Width = 429;
			// 
			// data_view_controller
			// 
			this.data_view_controller.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.data_view_controller.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
			this.data_view_controller.FullRowSelect = true;
			this.data_view_controller.GridLines = true;
			this.data_view_controller.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.data_view_controller.HideSelection = false;
			this.data_view_controller.Location = new System.Drawing.Point(12, 331);
			this.data_view_controller.MultiSelect = false;
			this.data_view_controller.Name = "data_view_controller";
			this.data_view_controller.Size = new System.Drawing.Size(648, 172);
			this.data_view_controller.TabIndex = 2;
			this.data_view_controller.UseCompatibleStateImageBehavior = false;
			this.data_view_controller.View = System.Windows.Forms.View.Details;
			this.data_view_controller.MouseClick += new System.Windows.Forms.MouseEventHandler(this.data_view_controller_MouseClick);
			this.data_view_controller.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.data_view_controller_MouseDoubleClick);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Controller";
			this.columnHeader1.Width = 116;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Key";
			this.columnHeader2.Width = 95;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Data";
			this.columnHeader3.Width = 429;
			// 
			// lMidiIn
			// 
			this.lMidiIn.AutoSize = true;
			this.lMidiIn.Location = new System.Drawing.Point(12, 9);
			this.lMidiIn.Name = "lMidiIn";
			this.lMidiIn.Size = new System.Drawing.Size(96, 13);
			this.lMidiIn.TabIndex = 3;
			this.lMidiIn.Text = "Midi In: [Unknown]";
			// 
			// btnAddNote
			// 
			this.btnAddNote.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnAddNote.Location = new System.Drawing.Point(12, 512);
			this.btnAddNote.Name = "btnAddNote";
			this.btnAddNote.Size = new System.Drawing.Size(145, 23);
			this.btnAddNote.TabIndex = 4;
			this.btnAddNote.Text = "Add Note Mapping";
			this.btnAddNote.UseVisualStyleBackColor = true;
			this.btnAddNote.Click += new System.EventHandler(this.btnAddNote_Click);
			// 
			// lMidiOut
			// 
			this.lMidiOut.AutoSize = true;
			this.lMidiOut.Location = new System.Drawing.Point(278, 9);
			this.lMidiOut.Name = "lMidiOut";
			this.lMidiOut.Size = new System.Drawing.Size(101, 13);
			this.lMidiOut.TabIndex = 5;
			this.lMidiOut.Text = "MidiOut: [Unknown]";
			// 
			// btnChangeMidiDev
			// 
			this.btnChangeMidiDev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnChangeMidiDev.Location = new System.Drawing.Point(318, 512);
			this.btnChangeMidiDev.Name = "btnChangeMidiDev";
			this.btnChangeMidiDev.Size = new System.Drawing.Size(127, 23);
			this.btnChangeMidiDev.TabIndex = 6;
			this.btnChangeMidiDev.Text = "Change Midi Devices";
			this.btnChangeMidiDev.UseVisualStyleBackColor = true;
			this.btnChangeMidiDev.Click += new System.EventHandler(this.btnChangeMidiDev_Click);
			// 
			// btnAddController
			// 
			this.btnAddController.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnAddController.Location = new System.Drawing.Point(163, 512);
			this.btnAddController.Name = "btnAddController";
			this.btnAddController.Size = new System.Drawing.Size(149, 23);
			this.btnAddController.TabIndex = 7;
			this.btnAddController.Text = "Add Controller Mapping";
			this.btnAddController.UseVisualStyleBackColor = true;
			this.btnAddController.Click += new System.EventHandler(this.btnAddController_Click);
			// 
			// btnSaveAll
			// 
			this.btnSaveAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnSaveAll.Location = new System.Drawing.Point(451, 512);
			this.btnSaveAll.Name = "btnSaveAll";
			this.btnSaveAll.Size = new System.Drawing.Size(127, 23);
			this.btnSaveAll.TabIndex = 8;
			this.btnSaveAll.Text = "Save Changes";
			this.btnSaveAll.UseVisualStyleBackColor = true;
			this.btnSaveAll.Click += new System.EventHandler(this.btnSaveAll_Click);
			// 
			// lLoopInterval
			// 
			this.lLoopInterval.AutoSize = true;
			this.lLoopInterval.Location = new System.Drawing.Point(516, 9);
			this.lLoopInterval.Name = "lLoopInterval";
			this.lLoopInterval.Size = new System.Drawing.Size(81, 13);
			this.lLoopInterval.TabIndex = 9;
			this.lLoopInterval.Text = "Loop Interval: 0";
			// 
			// Main
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(672, 543);
			this.Controls.Add(this.lLoopInterval);
			this.Controls.Add(this.btnSaveAll);
			this.Controls.Add(this.btnAddController);
			this.Controls.Add(this.btnChangeMidiDev);
			this.Controls.Add(this.lMidiOut);
			this.Controls.Add(this.btnAddNote);
			this.Controls.Add(this.lMidiIn);
			this.Controls.Add(this.data_view_controller);
			this.Controls.Add(this.data_view_note);
			this.Name = "Main";
			this.Text = "mhk configurator";
			this.Load += new System.EventHandler(this.Main_Load);
			this.ResizeEnd += new System.EventHandler(this.Main_ResizeEnd);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView data_view_note;
        private System.Windows.Forms.ColumnHeader chead_nt;
        private System.Windows.Forms.ColumnHeader chead_key;
        private System.Windows.Forms.ColumnHeader chead_spdata;
		private System.Windows.Forms.ListView data_view_controller;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.Label lMidiIn;
		private System.Windows.Forms.Button btnAddNote;
		private System.Windows.Forms.Label lMidiOut;
		private System.Windows.Forms.Button btnChangeMidiDev;
		private System.Windows.Forms.Button btnAddController;
		private System.Windows.Forms.Button btnSaveAll;
		private System.Windows.Forms.Label lLoopInterval;
	}
}

