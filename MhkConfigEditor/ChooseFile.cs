﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MhkConfigEditor
{
	public partial class ChooseFile : Form
	{
		public string Path { get; private set; } = "config.json";
		public ChooseFile()
		{
			InitializeComponent();
		}

		private void btnBrowse_Click(object sender, EventArgs e)
		{
			if (oDiag.ShowDialog() == DialogResult.OK)
			{
				textPath.Text = oDiag.FileName;
			}
		}

		private void btnOpen_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void ChooseFile_Load(object sender, EventArgs e)
		{
			textPath.Text = Path;
		}

		private void ChooseFile_FormClosing(object sender, FormClosingEventArgs e)
		{
			Path = textPath.Text;
		}
	}
}
