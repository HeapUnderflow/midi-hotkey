# Midi Hotkey

## Download

A reasonably up to date binary can be found [here](https://heap.wtf/static/midi_hk.exe)

## Command Line Interface

```text
midi_hk 0.1.0
HeapUnderflow

USAGE:
    midi_hk [OPTIONS]

OPTIONS:
    -c, --config <config>                    config file. can be either in yaml, json or ron format
        --example-config <example-config>    write a example config to the given path (.ron, .json or .yaml)
    -h, --help                               Prints help information
        --list-midi                          list all visible MIDI devices
    -l, --log-level <LEVEL>                  Log Level [possible values: error, warn, info, debug, trace]
    -i, --in <DEVICE>                        read from the specified midi device (overrides config)
    -o, --out <DEVICE>                       play back to the specified midi device (overrides config)
    -V, --version                            Prints version information
```
