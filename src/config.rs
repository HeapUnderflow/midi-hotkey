use crate::{
	keys::VirtualKey,
	midi::{controller::MidiController, note::MidiNote},
	utils::StringOrNumber,
};
use serde::{Deserialize, Serialize};
use snafu::{ResultExt, Snafu};
use std::{fs, io, path::Path};

#[inline]
const fn default_interval() -> u64 { 5 }

type ConfigResult<T> = Result<T, ConfigError>;

#[derive(Debug, Snafu)]
pub enum ConfigError {
	Io { source: io::Error },
	Json { source: serde_json::Error },
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Config {
	pub midi_in: StringOrNumber,
	pub midi_out: Option<StringOrNumber>,
	#[serde(default = "default_interval")]
	pub loop_interval: u64,

	pub notes:       Notes,
	pub controllers: Controllers,
}

pub type Notes = Vec<Note>;
#[derive(Deserialize, Serialize, Debug, Clone, Eq, PartialEq)]
pub struct Note {
	pub note:    MidiNote,
	pub keycode: VirtualKey,
	// pub event_type: Option<bool>,
	pub channel: u8,
	pub min_vel: u8,
	pub max_vel: u8,
	pub ara:     Option<u64>,
}

type Controllers = Vec<Controller>;
#[derive(Deserialize, Serialize, Debug, Clone, Eq, PartialEq)]
pub struct Controller {
	pub controller: MidiController,
	pub keycode:    VirtualKey,
	pub deadzone:   u8,
	pub midpoint:   u8,
	pub ara:        Option<u64>,
}

impl Config {
	pub fn dump_example<P: AsRef<Path>>(to: P) -> ConfigResult<()> {
		let ex = Config {
			midi_in:       StringOrNumber::Number(1),
			midi_out:      Some(StringOrNumber::String(String::from("fancy midi device"))),
			loop_interval: 1,
			notes:         vec![
				Note {
					note:    MidiNote::Cn,
					keycode: VirtualKey::VK_C,
					channel: 1,
					min_vel: 30,
					max_vel: 127,
					ara:     Some(1),
				},
				Note {
					note:    MidiNote::An,
					keycode: VirtualKey::VK_A,
					channel: 1,
					min_vel: 30,
					max_vel: 127,
					ara:     Some(500),
				},
			],
			controllers:   vec![Controller {
				controller: MidiController::FootController,
				keycode:    VirtualKey::VK_SPACE,
				deadzone:   10,
				midpoint:   127,
				ara:        Some(300),
			}],
		};

		let f = fs::File::create(to.as_ref()).context(Io)?;
		serde_json::to_writer_pretty(f, &ex).context(Json)
	}

	pub fn dump_empty<P: AsRef<Path>>(to: P) -> ConfigResult<()> {
		let ex = Config {
			midi_in:       StringOrNumber::Number(0),
			midi_out:      None,
			loop_interval: 5,
			notes:         Vec::default(),
			controllers:   Vec::default(),
		};

		let f = fs::File::create(to.as_ref()).context(Io)?;
		serde_json::to_writer_pretty(f, &ex).context(Json)
	}

	pub fn load<P: AsRef<Path>>(from: P) -> ConfigResult<Config> {
		let f = fs::File::open(from.as_ref()).context(Io)?;

		let data = match serde_json::from_reader(f) {
			Ok(res) => res,
			Err(e) => {
				log::error!("failed to load config from {}", from.as_ref().display());
				return Err(ConfigError::Json { source: e });
			},
		};

		Ok(data)
	}

	#[allow(dead_code)]
	pub fn save<P: AsRef<Path>>(&self, to: P) -> ConfigResult<()> {
		let f = fs::File::create(to.as_ref()).context(Io)?;
		serde_json::to_writer_pretty(f, &self).context(Json)
	}
}
