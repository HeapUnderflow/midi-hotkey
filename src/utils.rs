use serde::{de, Serialize};
use std::{
	fmt,
	io::{self},
};

/// Log an [`error`] if the given expression returns an Err
#[macro_export]
macro_rules! err_log {
	($e:expr) => {
		if let Err(why) = $e {
			log::error!(
				"[{}#{}:{}] {{ {} }} = {:?}",
				file!(),
				line!(),
				column!(),
				stringify!($e),
				why
				);
			}
	};
}

/// Enable ANSI Escape sequences on Windows shell.
///
/// This is a noop on unix or appel-like systems
#[cfg(target_os = "windows")]
pub fn set_con_mode() -> io::Result<()> {
	use winapi::um::{
		consoleapi::SetConsoleMode,
		processenv::GetStdHandle,
		winbase::STD_OUTPUT_HANDLE,
		wincon::{ENABLE_PROCESSED_OUTPUT, ENABLE_VIRTUAL_TERMINAL_PROCESSING},
		winnt::HANDLE,
	};

	let h_stdout: HANDLE;

	unsafe {
		h_stdout = GetStdHandle(STD_OUTPUT_HANDLE);
	}

	let r = unsafe {
		SetConsoleMode(
			h_stdout,
			ENABLE_PROCESSED_OUTPUT | ENABLE_VIRTUAL_TERMINAL_PROCESSING,
		)
	};

	if r == 0 {
		Err(io::Error::last_os_error())
	} else {
		Ok(())
	}
}

pub fn clamp2<T: PartialOrd>(l: T, r: T, v: T) -> T {
	if v < l {
		return l;
	}
	if v > r {
		return r;
	}
	v
}

/// Enable ANSI Escape sequences on Windows shell.
///
/// This is a noop on unix or appel-like systems
#[cfg(not(target_os = "windows"))]
pub fn set_con_mode() -> io::Result<()> { Ok(()) }

pub fn fern_setup(level: &str) -> Result<(), fern::InitError> {
	use fern::colors::{Color, ColoredLevelConfig};
	use log::LevelFilter as LF;

	let clr = ColoredLevelConfig::new()
		.trace(Color::BrightBlack)
		.debug(Color::White)
		.info(Color::BrightGreen)
		.warn(Color::BrightYellow)
		.error(Color::BrightRed);

	const SET: &str = "\x1B[90m";
	const RESET: &str = "\x1B[0m";

	let mut f = false;
	let lvl = match level.to_lowercase().as_str() {
		"error" => LF::Error,
		"warn" => LF::Warn,
		"info" => LF::Info,
		"debug" => LF::Debug,
		"trace" => LF::Trace,
		_ => {
			f = true;
			LF::Info
		},
	};

	fern::Dispatch::new()
		.format(move |out, message, record| {
			out.finish(format_args!(
				"{clb}[{cr}{} {:<5} {}{clb}]{cr} {}",
				chrono::Local::now().format("%FT%TZ"),
				clr.color(record.level()),
				record.target(),
				message,
				clb = SET,
				cr = RESET
			))
		})
		.level(lvl)
		.chain(std::io::stdout())
		.apply()?;
	if f {
		log::warn!("unknown logging level {:?}, defaulting to \"info\"", level);
	}
	Ok(())
}

#[derive(Serialize, Debug, Eq, PartialEq, Clone)]
#[serde(untagged)]
pub enum StringOrNumber {
	String(String),
	Number(u64),
}

impl<'de> de::Deserialize<'de> for StringOrNumber {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: de::Deserializer<'de>,
	{
		struct Visitor;

		impl<'de> de::Visitor<'de> for Visitor {
			type Value = StringOrNumber;

			fn expecting(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
				formatter.write_str("expected a string or a number")
			}

			fn visit_u64<E>(self, n: u64) -> Result<Self::Value, E>
			where
				E: de::Error,
			{
				Ok(StringOrNumber::Number(n))
			}

			fn visit_u32<E>(self, n: u32) -> Result<Self::Value, E>
			where
				E: de::Error,
			{
				Ok(StringOrNumber::Number(u64::from(n)))
			}

			fn visit_u16<E>(self, n: u16) -> Result<Self::Value, E>
			where
				E: de::Error,
			{
				Ok(StringOrNumber::Number(u64::from(n)))
			}

			fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
			where
				E: de::Error,
			{
				Ok(StringOrNumber::String(String::from(s)))
			}
		}

		deserializer.deserialize_any(Visitor)
	}
}

pub trait ResultWrappable: Sized {
	fn into_ok<E>(self) -> Result<Self, E> { Ok(self) }
	fn into_err<T>(self) -> Result<T, Self> { Err(self) }
}
