use crate::utils::StringOrNumber;
use midir::{
	ConnectError,
	InitError,
	MidiInput,
	MidiInputConnection,
	MidiOutput,
	MidiOutputConnection,
	PortInfoError,
};
use serde::{Deserialize, Serialize};
use snafu::{ResultExt, Snafu};
use std::fmt;

static MIDI_CLIENT_NAME: &str = concat!(env!("CARGO_PKG_NAME"), "/v", env!("CARGO_PKG_VERSION"));
static MIDI_LIST_CLIENT_NAME: &str = concat!(
	env!("CARGO_PKG_NAME"),
	"/v",
	env!("CARGO_PKG_VERSION"),
	" list-client"
);

#[derive(Debug, Snafu)]
pub enum MidiConnectError {
	DeviceNotFound,
	Init {
		source: InitError,
	},
	OutputConnect {
		source: ConnectError<MidiOutput>,
	},
	InputConnect {
		source: ConnectError<MidiInput>,
	},
	PortInfo {
		source: PortInfoError,
	},
	InvalidIdent {
		ident: MidiIdent,
	},
	InvalidMode {
		got:      DeviceMode,
		expected: DeviceMode,
	},
}

#[derive(Debug, Eq, PartialEq, Hash, Clone, Copy)]
pub enum DeviceMode {
	Input,
	Output,
}

pub type MidiConnectResult<T> = Result<T, MidiConnectError>;

#[derive(Debug, Eq, PartialEq, Hash, Clone)]
pub struct MidiIdent {
	pub num:  usize,
	pub name: String,
	pub mode: DeviceMode,
}

pub fn connect_output(
	ident: StringOrNumber,
) -> MidiConnectResult<(MidiOutputConnection, MidiIdent)> {
	let output = MidiOutput::new(MIDI_CLIENT_NAME).context(Init)?;

	let dev_ident = {
		let mut out = None;
		match ident {
			StringOrNumber::Number(port_num) => {
				let name = output.port_name(port_num as usize).context(PortInfo)?;
				out = Some(MidiIdent {
					num: port_num as usize,
					name,
					mode: DeviceMode::Output,
				});
			},
			StringOrNumber::String(port_name) => {
				for pn in 0..output.port_count() {
					let name = output.port_name(pn).context(PortInfo)?;

					if name == port_name {
						out = Some(MidiIdent {
							num: pn,
							name,
							mode: DeviceMode::Output,
						});
					}
				}
			},
		}
		match out {
			Some(s) => s,
			None => return Err(MidiConnectError::DeviceNotFound),
		}
	};

	let conn = output
		.connect(dev_ident.num, &dev_ident.name)
		.context(OutputConnect)?;

	Ok((conn, dev_ident))
}

pub fn connect_input<F>(
	ident: StringOrNumber,
	callback: F,
) -> MidiConnectResult<(MidiInputConnection<()>, MidiIdent)>
where
	F: FnMut(u64, &[u8], &mut ()) + Send + 'static,
{
	let mut input = MidiInput::new(MIDI_CLIENT_NAME).context(Init)?;
	input.ignore(midir::Ignore::None);

	let dev_ident = {
		let mut inp = None;
		match ident {
			StringOrNumber::Number(port_num) => {
				let name = input.port_name(port_num as usize).context(PortInfo)?;
				inp = Some(MidiIdent {
					num: port_num as usize,
					name,
					mode: DeviceMode::Input,
				});
			},
			StringOrNumber::String(port_name) => {
				for pn in 0..input.port_count() {
					let name = input.port_name(pn).context(PortInfo)?;

					if name == port_name {
						inp = Some(MidiIdent {
							num: pn,
							name,
							mode: DeviceMode::Input,
						});
					}
				}
			},
		}
		match inp {
			Some(s) => s,
			None => return Err(MidiConnectError::DeviceNotFound),
		}
	};

	let conn = input
		.connect(dev_ident.num, &dev_ident.name, callback, ())
		.context(InputConnect)?;

	Ok((conn, dev_ident))
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ListMidi {
	input:  Vec<String>,
	output: Vec<String>,
}

impl fmt::Display for ListMidi {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		f.write_str("## Available MIDI-IN ports ({} total):\n")?;
		for (i, dev) in self.input.iter().enumerate() {
			writeln!(f, "{:2}: {}", i, dev)?;
		}
		f.write_str("## Avaliable MIDI-OUT ports ({} total):\n")?;
		for (i, dev) in self.output.iter().enumerate() {
			writeln!(f, "{:2}: {}", i, dev)?;
		}
		Ok(())
	}
}

pub fn list_midi() -> ListMidi {
	// Using expect here since it **should** not really have the ability to fail (as its used from a sync context);
	let mut midi_in =
		MidiInput::new(MIDI_LIST_CLIENT_NAME).expect("Failed to create a midi reader");
	midi_in.ignore(midir::Ignore::None);

	let mut dev_in = Vec::new();
	for i in 0..midi_in.port_count() {
		dev_in.push(
			midi_in
				.port_name(i)
				.unwrap_or_else(|_| String::from("[ERROR: NO MIDID PORT NAME]")),
		);
	}

	let midi_out = MidiOutput::new(MIDI_LIST_CLIENT_NAME).expect("Failed to create a midi writer");

	let mut dev_out = Vec::new();
	for i in 0..midi_out.port_count() {
		dev_out.push(
			midi_out
				.port_name(i)
				.unwrap_or_else(|_| String::from("[ERROR: NO MIDID PORT NAME]")),
		);
	}

	ListMidi {
		input:  dev_in,
		output: dev_out,
	}
}
