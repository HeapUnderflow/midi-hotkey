#[macro_use]
mod utils;
mod config;
mod keys;
mod midi;
mod midi_connect;
mod run;

use clap::{App, AppSettings, Arg, ArgGroup};
use config::Config;
use crossbeam_channel::unbounded;

fn main() {
	if let Err(e) = utils::set_con_mode() {
		eprintln!(
			"failed to enable ansi escape sequences, output may look mangled\n{:?}",
			e
		);
	}

	let mut settings = vec![AppSettings::ColoredHelp, AppSettings::UnifiedHelpMessage];
	if cfg!(target_os = "windows") {
		settings.push(AppSettings::ColorAlways)
	} else {
		settings.push(AppSettings::ColorAuto)
	}
	if std::env::var("MHK_WAIT_ON_ERROR")
		.map(|v| v == "1")
		.unwrap_or_else(|_| false)
	{
		settings.push(AppSettings::WaitOnError);
	}

	let matches = App::new(env!("CARGO_PKG_NAME"))
		.author(env!("CARGO_PKG_AUTHORS"))
		.version(env!("CARGO_PKG_VERSION"))
		.settings(&settings)
		.bin_name("midi_hk")
		.group(ArgGroup::with_name("misc").multiple(false))
		.arg(
			Arg::with_name("example-config")
				.long("example-config")
				.takes_value(true)
				.help("Write a example config to the given path")
				.group("misc"),
		)
		.arg(
			Arg::with_name("list-midi")
				.long("list-midi")
				.takes_value(true)
				.possible_values(&["plain", "json"])
				.case_insensitive(true)
				.help("List all visible MIDI devices")
				.group("misc"),
		)
		.arg(
			Arg::with_name("log-level")
				.short("l")
				.long("log-level")
				.takes_value(true)
				.value_name("LEVEL")
				.default_value("info")
				.possible_values(&["error", "warn", "info", "debug", "trace"])
				.hide_default_value(false)
				.hide_possible_values(false)
				.help("Log Level"),
		)
		.arg(
			Arg::with_name("config")
				.short("c")
				.long("config")
				.takes_value(true)
				.hide_default_value(true)
				.help("Use config file")
				.default_value("config.json")
				.value_name("CONFIG")
				.hide_default_value(false),
		)
		.arg(
			Arg::with_name("midi-out")
				.short("o")
				.long("out")
				.takes_value(true)
				.help("play back to the specified midi device (overrides config)")
				.value_name("DEVICE"),
		)
		.arg(
			Arg::with_name("midi-in")
				.short("i")
				.long("in")
				.takes_value(true)
				.help("read from the specified midi device (overrides config)")
				.value_name("DEVICE"),
		)
		.arg(
			Arg::with_name("dump-empty")
				.long("dump-empty")
				.hidden(true)
				.takes_value(true)
				.help("Dump an empty config file to path")
				.value_name("PATH"),
		)
		.arg(
			Arg::with_name("loop-interval")
				.long("loop-interval")
				.takes_value(true)
				.value_name("INTERVAL")
				.help(
					"Specify the loop interval in ms. Overrides config. Lower means faster key \
					 responses but higher CPU usage",
				),
		)
		.get_matches();

	utils::fern_setup(matches.value_of("log-level").unwrap_or(""))
		.expect("logger failed to initialize");

	// config::Config::dump_example("config.json").expect("wat");
	if let Some(mode) = matches.value_of_lossy("list-midi") {
		log::debug!("listing midi");

		let lm = crate::midi_connect::list_midi();
		match mode.to_ascii_lowercase().as_ref() {
			"json" => match serde_json::to_string(&lm) {
				Ok(v) => println!(r#"{{"status": "ok", "data": {}}}"#, v),
				Err(_) => println!(r#"{{"status": "error"}}"#),
			},
			"plain" => println!("{}", lm),
			_ => unreachable!("not all arg options where covered"),
		};

		return;
	}

	if let Some(ex_cfg_path) = matches.value_of_lossy("example-config") {
		log::info!("writing example to: {}", ex_cfg_path);
		err_log!(Config::dump_example(&*ex_cfg_path));
		return;
	}

	if let Some(empty_path) = matches.value_of_lossy("dump-empty") {
		log::info!("dumping empty config to: {}", empty_path);
		err_log!(Config::dump_empty(&*empty_path));
		return;
	}

	// remove mutability off config
	let config = match load_config(&matches) {
		Ok(cfg) => cfg,
		Err(_) => return,
	};

	log::trace!("loaded config:\n{:#?}", config);

	let (tx, rx) = unbounded();
	let runloop = run::RunLoop::new(100, rx, &config.notes, &config.controllers);

	let mut midi_out = config
		.midi_out
		.and_then(|val| match midi_connect::connect_output(val) {
			Ok((out, _)) => Some(out),
			Err(e) => {
				log::error!("failed to connect to midi-out: {}", e);
				log::error!("ignoring midi out");
				None
			},
		});
	let _midi_in = match midi_connect::connect_input(config.midi_in, move |ts, msg, dt| {
		handle_midi_msg(ts, msg, dt, &mut midi_out, &tx);
	}) {
		Ok((inp, _)) => inp,
		Err(e) => {
			log::error!("failed to connect to midi-in: {}", e);
			return;
		},
	};

	println!("All configs parsed!");
	println!("We will now listen to events");

	// this is blocking,
	// if any error occours this will exit, and thus exit the program
	runloop.run_forever();
	log::warn!("runloop exited");
}

fn load_config(matches: &clap::ArgMatches) -> Result<Config, ()> {
	let mut config = match Config::load(
		&*matches
			.value_of_lossy("config")
			.expect("missing default value for config"),
	) {
		Ok(cfg) => cfg,
		Err(e) => {
			log::error!("error occured while loading configuration file");
			log::error!("{:?}", e);
			return Err(());
		},
	};

	if let Some(val) = matches.value_of("midi-in") {
		match val.parse() {
			Ok(v) => config.midi_in = crate::utils::StringOrNumber::Number(v),
			Err(_) => config.midi_in = crate::utils::StringOrNumber::String(String::from(val)),
		}
	}

	if let Some(val) = matches.value_of("midi-out") {
		match val.parse() {
			Ok(v) => config.midi_out = Some(crate::utils::StringOrNumber::Number(v)),
			Err(_) => {
				config.midi_out = Some(crate::utils::StringOrNumber::String(String::from(val)))
			},
		}
	}

	match matches
		.value_of_lossy("loop-interval")
		.map(|val| val.parse())
	{
		Some(Ok(v)) if v >= 1 => config.loop_interval = v,
		Some(Ok(_)) => log::warn!("interval cant be 0, ignoring option"),
		Some(Err(e)) => log::warn!(
			"unable to parse interval as a number (max: {}): {}",
			u64::max_value(),
			e
		),
		None => (),
	}

	for v in config.notes.iter_mut() {
		// clamp the velocity values to reasonable levels
		v.max_vel = utils::clamp2(0, 127, v.max_vel);
		v.min_vel = utils::clamp2(0, 127, v.min_vel);
	}

	Ok(config)
}

// TODO: Make this a bit more elegant
fn handle_midi_msg(
	_: u64,
	msg: &[u8],
	_: &mut (),
	midi_out: &mut Option<midir::MidiOutputConnection>,
	tx: &crossbeam_channel::Sender<crate::midi::MidiMessage>,
) {
	if let Some(ref mut out) = midi_out {
		match out.send(msg) {
			Ok(()) => (),
			Err(e) => log::warn!("failed to send midi-message to output: {}", e),
		}
	}

	match crate::midi::MidiMessage::new(msg) {
		Ok(message) => {
			log::trace!("Read({:02X?}) => {:02X?}", msg, message);
			err_log!(tx.send(message));
		},
		Err(e) => log::debug!("unknown midi message: {:02X?}", e),
	}
}
