use super::MidiError;
use serde::{de, Serialize};
use std::fmt;

#[derive(Debug, PartialOrd, PartialEq, Eq, Copy, Clone, Serialize, Hash)]
pub enum MidiController {
	Modulation,
	Breath,
	FootController,
	PortamentoTime,
	Volume,
	Balance,
	Pan,
	Expression,
	PedalSustain,
	Portamento,
	PedalSostenuto,
	PedalSoft,
	Hold2,
	ExternalEffectsDepth,
	TremoloDepth,
	ChorusDepth,
	CelesteDetuneDepth,
	PhaserDepth,
	Other(u8),
}

impl MidiController {
	pub fn new(v: u8) -> Result<MidiController, MidiError> {
		use MidiController::*;
		if v > 127 {
			return Err(MidiError::Unimplemented { byte: v });
		}

		match v {
			1 => Ok(Modulation),
			2 => Ok(Breath),
			4 => Ok(FootController),
			5 => Ok(PortamentoTime),
			7 => Ok(Volume),
			8 => Ok(Balance),
			10 => Ok(Pan),
			11 => Ok(Expression),
			64 => Ok(PedalSustain),
			65 => Ok(Portamento),
			66 => Ok(PedalSostenuto),
			67 => Ok(PedalSoft),
			69 => Ok(Hold2),
			91 => Ok(ExternalEffectsDepth),
			92 => Ok(TremoloDepth),
			93 => Ok(ChorusDepth),
			94 => Ok(CelesteDetuneDepth),
			95 => Ok(PhaserDepth),
			other => Ok(Other(other)),
		}
	}

	pub fn parse(text: &str) -> Result<MidiController, MidiError> {
		use MidiController::*;
		match text.to_lowercase().as_str() {
			"modulation" => Ok(Modulation),
			"breath" => Ok(Breath),
			"foot_controller" | "footcontroller" => Ok(FootController),
			"portamento_time" | "portamentotime" => Ok(PortamentoTime),
			"volume" => Ok(Volume),
			"balance" => Ok(Balance),
			"pan" => Ok(Pan),
			"expression" => Ok(Expression),
			"pedal_sustain" | "pedalsustain" => Ok(PedalSustain),
			"portamento" => Ok(Portamento),
			"pedal_sostenuto" | "pedalsostenuto" => Ok(PedalSostenuto),
			"pedal_soft" | "pedalsoft" => Ok(PedalSoft),
			"hold2" => Ok(Hold2),
			"external_effects_depth" | "externaleffectsdepth" => Ok(ExternalEffectsDepth),
			"tremolo_depth" | "tremolodepth" => Ok(TremoloDepth),
			"chorus_depth" | "chorusdepth" => Ok(ChorusDepth),
			"celeste_detune_depth" | "celestedetunedepth" => Ok(CelesteDetuneDepth),
			"phaser_depth" | "phaserdepth" => Ok(PhaserDepth),
			_ => Err(MidiError::Unparseable),
		}
	}

	// NOTE: Allow dead code for completness reasons
	#[allow(dead_code)]
	pub fn as_u8(self) -> u8 {
		use MidiController::*;
		match self {
			Modulation => 1,
			Breath => 2,
			FootController => 4,
			PortamentoTime => 5,
			Volume => 7,
			Balance => 8,
			Pan => 10,
			Expression => 11,
			PedalSustain => 64,
			Portamento => 65,
			PedalSostenuto => 66,
			PedalSoft => 67,
			Hold2 => 69,
			ExternalEffectsDepth => 91,
			TremoloDepth => 92,
			ChorusDepth => 93,
			CelesteDetuneDepth => 94,
			PhaserDepth => 95,
			Other(v) => v,
		}
	}
}

impl<'de> de::Deserialize<'de> for MidiController {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: de::Deserializer<'de>,
	{
		struct Visitor;

		impl<'de> de::Visitor<'de> for Visitor {
			type Value = MidiController;

			fn expecting(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
				formatter.write_str("a valid controller")
			}

			fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
			where
				E: de::Error,
			{
				match MidiController::parse(s) {
					Ok(v) => Ok(v),
					Err(e) => match e {
						MidiError::NoteOutOfRange => Err(E::custom("note out of range")),
						MidiError::Unimplemented { byte: val } => Err(E::invalid_value(
							serde::de::Unexpected::Unsigned(u64::from(val)),
							&"value between 0 and 127",
						)),
						MidiError::Unparseable => Err(E::custom("unparsable")),
						MidiError::TooShort => Err(E::custom("too short")),
					},
				}
			}

			fn visit_u8<E>(self, value: u8) -> Result<Self::Value, E>
			where
				E: de::Error,
			{
				match MidiController::new(value) {
					Ok(v) => Ok(v),
					Err(e) => match e {
						MidiError::NoteOutOfRange => Err(E::custom("note out of range")),
						MidiError::Unimplemented { byte: val } => Err(E::invalid_value(
							serde::de::Unexpected::Unsigned(u64::from(val)),
							&"value between 0 and 127",
						)),
						MidiError::Unparseable => Err(E::custom("unparsable")),
						MidiError::TooShort => Err(E::custom("too short")),
					},
				}
			}
		}

		deserializer.deserialize_any(Visitor)
	}
}
