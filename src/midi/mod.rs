pub mod controller;
pub mod note;

use self::{controller::MidiController, note::MidiNote};
use snafu::Snafu;

#[derive(Debug, PartialEq, Snafu)]
#[snafu(visibility(pub(self)))]
pub enum MidiError {
	#[snafu(display("the recieved midi event is too short"))]
	TooShort,
	#[snafu(display("the recieved byte 0x{:02X} is unknown", byte))]
	Unimplemented { byte: u8 },
	#[snafu(display("the recieved note is out of range"))]
	NoteOutOfRange,
	#[snafu(display("the recieved event is unparsable"))]
	Unparseable,
}

pub type MidiResult<T> = Result<T, MidiError>;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum MidiEvent {
	NoteOn,
	NoteOff,
}

#[derive(Debug)]
pub enum MidiMessage {
	Note(MidiNoteMessage),
	Controller(MidiControllerMessage),
}

#[derive(Debug)]
pub struct MidiControllerMessage {
	pub controller: MidiController,
	pub value:      u8,
}

#[derive(Debug)]
pub struct MidiNoteMessage {
	pub event:    MidiEvent,
	pub channel:  u8,
	pub note:     MidiNote,
	pub velocity: u8,
}

impl MidiMessage {
	pub fn new(message: &[u8]) -> MidiResult<MidiMessage> {
		match message[0] & 0xf0 {
			0x80 => {
				if message.len() < 3 {
					Err(MidiError::TooShort)
				} else {
					Ok(MidiMessage::Note(MidiNoteMessage {
						event:    MidiEvent::NoteOff,
						channel:  message[0] & 0x0f,
						note:     MidiNote::new(message[1] & 0x7f)?,
						velocity: message[2] & 0x7f,
					}))
				}
			},
			0x90 => {
				if message.len() < 3 {
					Err(MidiError::TooShort)
				} else {
					let velocity = message[2] & 0x7f;
					let event = if velocity != 0 {
						MidiEvent::NoteOn
					} else {
						MidiEvent::NoteOff
					};
					Ok(MidiMessage::Note(MidiNoteMessage {
						event,
						channel: message[0] & 0x0f,
						note: MidiNote::new(message[1] & 0x7f)?,
						velocity,
					}))
				}
			},
			0xB0 => {
				if message.len() < 3 {
					Err(MidiError::TooShort)
				} else {
					Ok(MidiMessage::Controller(MidiControllerMessage {
						controller: MidiController::new(message[1] & 0x7f)?,
						value:      message[2] & 0x7f,
					}))
				}
			},
			_ => Err(MidiError::Unimplemented { byte: message[0] }),
		}
	}
}
