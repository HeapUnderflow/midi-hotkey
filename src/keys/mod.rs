// pub mod scancode;
mod virtual_key;
pub use _keys_impl::*;
pub use virtual_key::*;

#[cfg(target_os = "windows")]
mod _keys_impl {
	use super::VirtualKey;
	use std::{convert::TryInto, io, mem};
	use winapi::{
		shared::minwindef::WORD,
		um::winuser::{INPUT_u, SendInput, INPUT, INPUT_KEYBOARD, KEYEVENTF_KEYUP},
	};

	pub fn send_key_down(key: VirtualKey) -> Result<(), io::Error> {
		let mut i = INPUT {
			type_: INPUT_KEYBOARD,
			u:     unsafe {
				let mut iu: INPUT_u = mem::zeroed();
				let ki = iu.ki_mut();

				ki.time = 0;
				ki.wVk = key as WORD;
				ki.dwExtraInfo = 0;
				ki.dwFlags = 0;
				ki.wScan = 0;
				iu
			},
		};

		let r = unsafe {
			SendInput(
				1,
				&mut i,
				mem::size_of_val(&i)
					.try_into()
					.expect("INPUT structure too large"),
			)
		};

		if r == 0 {
			Err(io::Error::last_os_error())
		} else {
			Ok(())
		}
	}

	pub fn send_key_up(key: VirtualKey) -> Result<(), io::Error> {
		let mut i = INPUT {
			type_: INPUT_KEYBOARD,
			u:     unsafe {
				let mut iu: INPUT_u = mem::zeroed();
				let ki = iu.ki_mut();

				ki.time = 0;
				ki.wVk = key as WORD;
				ki.dwExtraInfo = 0;
				ki.dwFlags = KEYEVENTF_KEYUP;
				ki.wScan = 0;
				iu
			},
		};

		let r = unsafe {
			SendInput(
				1,
				&mut i,
				mem::size_of_val(&i)
					.try_into()
					.expect("INPUT structure too large"),
			)
		};

		if r == 0 {
			Err(io::Error::last_os_error())
		} else {
			Ok(())
		}
	}
}

#[cfg(not(target_os = "windows"))]
mod _keys_impl {
	use super::VirtualKey;
	use std::io;

	pub fn send_key_down(_: VirtualKey) -> Result<(), io::Error> {
		log::warn!("send_key_down was compiled as a stub and will do nothing (E_NOT_WINDOWS)");
		Ok(())
	}

	pub fn send_key_up(_: VirtualKey) -> Result<(), io::Error> {
		log::warn!("send_key_up was compiled as a stub and will do nothing (E_NOT_WINDOWS)");
		Ok(())
	}
}
