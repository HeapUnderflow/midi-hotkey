// TODO: Remove this
#![allow(dead_code)]

use serde::{de, ser};
use std::{fmt, io};

macro_rules! gen_vk_forbidden_filter_match {
	(map($v:ident) {
		RESERVED $($rs:expr),*;
		UNKNOWN $($es:expr),*;
		VALID $d:expr
	}) => {{
		match $v {
			$(
				$rs => Err(Error::Reserved),
			)*
			$(
				$es => Err(Error::UnknownByte),
			)*
			_ => Ok($d)
		}
	}};
}

// \s+(VK_[A-Za-z0-9_]+)\s+(=\s+0x[0-9A-Z]+)?,\s+// .*$

#[repr(u8)]
#[derive(Debug, Clone, Copy, Hash, Eq, PartialEq, Ord, PartialOrd)]
#[allow(non_camel_case_types)]
pub enum VirtualKey {
	VK_BACK    = 0x08, // Backspace
	VK_TAB     = 0x09, // Tab
	VK_CLEAR   = 0x0C, // Clear
	VK_RETURN  = 0x0D, // Enter
	VK_SHIFT   = 0x10, // Shift
	VK_CONTROL = 0x11, // Ctrl
	VK_MENU    = 0x12, // Alt,
	VK_PAUSE   = 0x13, // Pause,
	VK_CAPITAL = 0x14, // Caps-Lock
	// Leaving out named IME keys (except VK_ACCEPT)
	// This is due to them having different meanings but the same
	// Scan code. They are included as VK_IME_[VC]
	VK_IME_15  = 0x15, // Kana, Hangul,
	VK_IME_17  = 0x17, // Junja
	VK_IME_18  = 0x18, // "Final Mode"
	VK_IME_19  = 0x19, // Hanja, Kanji

	VK_ESCAPE  = 0x1B, // Escape
	VK_ACCEPT  = 0x1E, // IME Accept
	VK_SPACE   = 0x20, // Spacebar
	VK_PRIOR,          // PgUp,
	VK_NEXT,           // PgDown,
	VK_END,            // End
	VK_HOME,           // Home
	VK_LEFT,           // Left Arrow
	VK_UP,             // Up Arrow
	VK_RIGHT,          // Right Arrow
	VK_DOWN,           // Down Arrow,
	VK_SELECT,         // Select
	VK_PRINT,          // Print
	VK_EXECUTE,        // Execute,
	VK_SNAPSHOT,       // PrintScr
	VK_INSERT,         // Insert
	VK_DELETE,         // Delete
	VK_HELP,           // Help

	// Usual Number Range
	VK_ZERO    = 0x30, // 0
	VK_ONE,            // 1
	VK_TWO,            // 2
	VK_THREE,          // 3
	VK_FOUR,           // 4
	VK_FIVE,           // 5
	VK_SIX,            // 6
	VK_SEVEN,          // 7
	VK_EIGHT,          // 8
	VK_NINE,           // 9

	VK_A       = 0x41, // A
	VK_B,              // B
	VK_C,              // C
	VK_D,              // D
	VK_E,              // E
	VK_F,              // F
	VK_G,              // G
	VK_H,              // H
	VK_I,              // I
	VK_J,              // J
	VK_K,              // K
	VK_L,              // L
	VK_M,              // M
	VK_N,              // N
	VK_O,              // O
	VK_P,              // P
	VK_Q,              // Q
	VK_R,              // R
	VK_S,              // S
	VK_T,              // T
	VK_U,              // U
	VK_V,              // V
	VK_W,              // W
	VK_X,              // X
	VK_Y,              // Y
	VK_Z,              // Z

	VK_LWIN    = 0x5B, // Left Windows Key (Natural KB)
	VK_RWIN    = 0x5C, // Right Windows Key (Natural KB)
	VK_APPS    = 0x5D, // Apps
	VK_SLEEP   = 0x5F, // "Sleep"

	VK_NUMPAD0 = 0x60,
	VK_NUMPAD1,
	VK_NUMPAD2,
	VK_NUMPAD3,
	VK_NUMPAD4,
	VK_NUMPAD5,
	VK_NUMPAD6,
	VK_NUMPAD7,
	VK_NUMPAD8,
	VK_NUMPAD9,

	VK_MULTIPLY = 0x6A, // Nupad *
	VK_ADD,             // Numpad +
	VK_SEPARATOR,       // Numpad . (, US)
	VK_SUBSTRACT,       // Numpad -
	VK_DECIMAL,         // Numpad , (. US)
	VK_DIVIDE,          // Numpad /

	VK_F1      = 0x70,
	VK_F2,
	VK_F3,
	VK_F4,
	VK_F5,
	VK_F6,
	VK_F7,
	VK_F8,
	VK_F9,
	VK_F10,
	VK_F11,
	VK_F12,
	VK_F13,
	VK_F14,
	VK_F15,
	VK_F16,
	VK_F17,
	VK_F18,
	VK_F19,
	VK_F20,
	VK_F21,
	VK_F22,
	VK_F23,
	VK_F24,

	VK_NUMLOCK = 0x90,
	VK_SCROLL  = 0x91,

	VK_LSHIFT  = 0xA0, // Left Shift
	VK_RSHIFT,
	VK_LCONTROL,
	VK_RCONTROL,
	VK_LMENU,
	VK_RMENU,
	VK_BROWSER_BACK,
	VK_BROWSER_FORWARD,
	VK_BROWSER_REFRESH,
	VK_BROWSER_STOP,
	VK_BROWSER_SEARCH,
	VK_BROWSER_FAVORITES,
	VK_BROWSER_HOME,
	VK_VOLUME_MUTE,
	VK_VOLUME_DOWN,
	VK_VOLUME_UP,
	VK_MEDIA_NEXT_TRACK,
	VK_MEDIA_PREV_TRACK,
	VK_MEDIA_STOP,
	VK_MEDIA_PLAY_PAUSE,
	VK_LAUNCH_MAIL,
	VK_LAUNCH_MEDIA_SELECT,
	VK_LAUNCH_APP1,
	VK_LAUNCH_APP2,

	VK_OEM_1   = 0xBA, // Misc, standard US `:;`
	VK_OEM_PLUS,       // For any country/region, the `+` key
	VK_OEM_COMMA,      // For any country/region, the `,` key
	VK_OEM_MINUS,      // For any country/region the `-` key
	VK_OEM_PERIOD,     // For any country/region the `.` key
	VK_OEM_2,          // Misc, standard US `/?`
	VK_OEM_3,          // Misc, standard US `~`

	VK_OEM_4   = 0xDB,           // Misc, standard US `[{`
	VK_OEM_5,                    // Misc, standard US `\|`
	VK_OEM_6,                    // Misc, standard US `]}`
	VK_OEM_7,                    // Misc, standard US `'"`
	VK_OEM_8,                    // Misc
	VK_OEM_SPECIFIC_0xE1 = 0xE1, // OEM Key
	VK_OEM_102 = 0xE2, /* Either the angle bracket key or the backslash key on the RT 102-key keyboard */
	VK_OEM_SPECIFIC_0xE3 = 0xE3, // OEM Key
	VK_OEM_SPECIFIC_0xE4 = 0xE4, // OEM Key

	VK_PROCESSKEY = 0xE5,        // IME Process key
	VK_OEM_SPECIFIC_0xE6 = 0xE6, // OEM Key
	// -- VK Packet is left out because i dont (yet) want to support
	// -- Unicode output for the hotkeys
	// VK_PACKET = 0xE7,
	VK_OEM_SPECIFIC_0xE9 = 0xE9, // OEM Key
	VK_OEM_SPECIFIC_0xF0,        // OEM Key
	VK_OEM_SPECIFIC_0xF1,        // OEM Key
	VK_OEM_SPECIFIC_0xF2,        // OEM Key
	VK_OEM_SPECIFIC_0xF3,        // OEM Key
	VK_OEM_SPECIFIC_0xF4,        // OEM Key
	VK_OEM_SPECIFIC_0xF5,        // OEM Key

	VK_ATTN    = 0xF6,
	VK_CRSEL,
	VK_EXSEL,
	VK_EREOF,
	VK_PLAY,
	VK_ZOOM,
	// VK_NONAME // Reserved key, skipping,
	VK_PA1     = 0xFD,
	VK_OEM_CLEAR = 0xFE,
}

impl VirtualKey {
	pub fn as_byte(self) -> u8 { self as u8 }

	/// Returns the Virtual Key as a ScanCode.
	///
	/// ## Errors
	/// Will return an error if no scancode exists for the
	/// VC
	pub fn as_scancode(self) -> io::Result<u32> { Self::map_vk_vsc(self as u32) }

	#[cfg(target_os = "windows")]
	fn map_vk_vsc(vk: u32) -> io::Result<u32> {
		// This will work as long as UINT == u32
		use winapi::um::winuser::{MapVirtualKeyW, MAPVK_VK_TO_VSC};
		let k = unsafe { MapVirtualKeyW(vk, MAPVK_VK_TO_VSC) };

		if k == 0 {
			Err(io::Error::last_os_error())
		} else {
			Ok(k)
		}
	}

	#[cfg(not(target_os = "windows"))]
	fn map_vk_vsc(_: u32) -> io::Result<u32> {
		Err(io::Error::new(io::ErrorKind::Other, "NOT_A_WINDOW"))
	}

	// Allowing complexity here, as most of this is macro generated
	#[allow(clippy::cognitive_complexity)]
	pub fn new(v: u8) -> Result<VirtualKey, Error> {
		use std::mem;
		gen_vk_forbidden_filter_match! {
			map(v) {
				RESERVED
					0x0A, 0x0B, 0x5E, 0x92, 0x93, 0x94,
					0x95, 0x96, 0xB8, 0xB9, 0xC1, 0xC2,
					0xC3, 0xC4, 0xC5, 0xC6, 0xC7, 0xC8,
					0xC9, 0xCA, 0xCB, 0xCC, 0xCD, 0xCE,
					0xCF, 0xD0, 0xD1, 0xD2, 0xD3, 0xD4,
					0xD5, 0xD6, 0xD7, 0xE0, 0xFC;
				UNKNOWN
					0x07, 0x0E, 0x0F, 0x16, 0x1A, 0x3A,
					0x3B, 0x3C, 0x3D, 0x3E, 0x3F, 0x40,
					0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D,
					0x8E, 0x8F, 0x97, 0x98, 0x99, 0x9A,
					0x9B, 0x9C, 0x9D, 0x9E, 0x9F, 0xD8,
					0xD9, 0xDA, 0xE8, 0xFF, /* VK_PACKET */ 0xE7;
				VALID
					unsafe { mem::transmute(v) }
			}
		}
	}

	pub fn parse(s: &str) -> Result<VirtualKey, Error> {
		match s.trim().to_uppercase().as_str() {
			"BACK" => Ok(VirtualKey::VK_BACK),
			"TAB" => Ok(VirtualKey::VK_TAB),
			"CLEAR" => Ok(VirtualKey::VK_CLEAR),
			"RETURN" => Ok(VirtualKey::VK_RETURN),
			"SHIFT" => Ok(VirtualKey::VK_SHIFT),
			"CONTROL" => Ok(VirtualKey::VK_CONTROL),
			"MENU" => Ok(VirtualKey::VK_MENU),
			"PAUSE" => Ok(VirtualKey::VK_PAUSE),
			"CAPITAL" => Ok(VirtualKey::VK_CAPITAL),
			"IME_15" => Ok(VirtualKey::VK_IME_15),
			"IME_17" => Ok(VirtualKey::VK_IME_17),
			"IME_18" => Ok(VirtualKey::VK_IME_18),
			"IME_19" => Ok(VirtualKey::VK_IME_19),
			"ESCAPE" => Ok(VirtualKey::VK_ESCAPE),
			"ACCEPT" => Ok(VirtualKey::VK_ACCEPT),
			"SPACE" => Ok(VirtualKey::VK_SPACE),
			"PRIOR" => Ok(VirtualKey::VK_PRIOR),
			"NEXT" => Ok(VirtualKey::VK_NEXT),
			"END" => Ok(VirtualKey::VK_END),
			"HOME" => Ok(VirtualKey::VK_HOME),
			"LEFT" => Ok(VirtualKey::VK_LEFT),
			"UP" => Ok(VirtualKey::VK_UP),
			"RIGHT" => Ok(VirtualKey::VK_RIGHT),
			"DOWN" => Ok(VirtualKey::VK_DOWN),
			"SELECT" => Ok(VirtualKey::VK_SELECT),
			"PRINT" => Ok(VirtualKey::VK_PRINT),
			"EXECUTE" => Ok(VirtualKey::VK_EXECUTE),
			"SNAPSHOT" => Ok(VirtualKey::VK_SNAPSHOT),
			"INSERT" => Ok(VirtualKey::VK_INSERT),
			"DELETE" => Ok(VirtualKey::VK_DELETE),
			"HELP" => Ok(VirtualKey::VK_HELP),
			"ZERO" | "0" => Ok(VirtualKey::VK_ZERO),
			"ONE" | "1" => Ok(VirtualKey::VK_ONE),
			"TWO" | "2" => Ok(VirtualKey::VK_TWO),
			"THREE" | "3" => Ok(VirtualKey::VK_THREE),
			"FOUR" | "4" => Ok(VirtualKey::VK_FOUR),
			"FIVE" | "5" => Ok(VirtualKey::VK_FIVE),
			"SIX" | "6" => Ok(VirtualKey::VK_SIX),
			"SEVEN" | "7" => Ok(VirtualKey::VK_SEVEN),
			"EIGHT" | "8" => Ok(VirtualKey::VK_EIGHT),
			"NINE" | "9" => Ok(VirtualKey::VK_NINE),
			"A" => Ok(VirtualKey::VK_A),
			"B" => Ok(VirtualKey::VK_B),
			"C" => Ok(VirtualKey::VK_C),
			"D" => Ok(VirtualKey::VK_D),
			"E" => Ok(VirtualKey::VK_E),
			"F" => Ok(VirtualKey::VK_F),
			"G" => Ok(VirtualKey::VK_G),
			"H" => Ok(VirtualKey::VK_H),
			"I" => Ok(VirtualKey::VK_I),
			"J" => Ok(VirtualKey::VK_J),
			"K" => Ok(VirtualKey::VK_K),
			"L" => Ok(VirtualKey::VK_L),
			"M" => Ok(VirtualKey::VK_M),
			"N" => Ok(VirtualKey::VK_N),
			"O" => Ok(VirtualKey::VK_O),
			"P" => Ok(VirtualKey::VK_P),
			"Q" => Ok(VirtualKey::VK_Q),
			"R" => Ok(VirtualKey::VK_R),
			"S" => Ok(VirtualKey::VK_S),
			"T" => Ok(VirtualKey::VK_T),
			"U" => Ok(VirtualKey::VK_U),
			"V" => Ok(VirtualKey::VK_V),
			"W" => Ok(VirtualKey::VK_W),
			"X" => Ok(VirtualKey::VK_X),
			"Y" => Ok(VirtualKey::VK_Y),
			"Z" => Ok(VirtualKey::VK_Z),
			"LWIN" => Ok(VirtualKey::VK_LWIN),
			"RWIN" => Ok(VirtualKey::VK_RWIN),
			"APPS" => Ok(VirtualKey::VK_APPS),
			"SLEEP" => Ok(VirtualKey::VK_SLEEP),
			"NUMPAD0" => Ok(VirtualKey::VK_NUMPAD0),
			"NUMPAD1" => Ok(VirtualKey::VK_NUMPAD1),
			"NUMPAD2" => Ok(VirtualKey::VK_NUMPAD2),
			"NUMPAD3" => Ok(VirtualKey::VK_NUMPAD3),
			"NUMPAD4" => Ok(VirtualKey::VK_NUMPAD4),
			"NUMPAD5" => Ok(VirtualKey::VK_NUMPAD5),
			"NUMPAD6" => Ok(VirtualKey::VK_NUMPAD6),
			"NUMPAD7" => Ok(VirtualKey::VK_NUMPAD7),
			"NUMPAD8" => Ok(VirtualKey::VK_NUMPAD8),
			"NUMPAD9" => Ok(VirtualKey::VK_NUMPAD9),
			"MULTIPLY" => Ok(VirtualKey::VK_MULTIPLY),
			"ADD" => Ok(VirtualKey::VK_ADD),
			"SEPARATOR" => Ok(VirtualKey::VK_SEPARATOR),
			"SUBSTRACT" => Ok(VirtualKey::VK_SUBSTRACT),
			"DECIMAL" => Ok(VirtualKey::VK_DECIMAL),
			"DIVIDE" => Ok(VirtualKey::VK_DIVIDE),
			"F1" => Ok(VirtualKey::VK_F1),
			"F2" => Ok(VirtualKey::VK_F2),
			"F3" => Ok(VirtualKey::VK_F3),
			"F4" => Ok(VirtualKey::VK_F4),
			"F5" => Ok(VirtualKey::VK_F5),
			"F6" => Ok(VirtualKey::VK_F6),
			"F7" => Ok(VirtualKey::VK_F7),
			"F8" => Ok(VirtualKey::VK_F8),
			"F9" => Ok(VirtualKey::VK_F9),
			"F10" => Ok(VirtualKey::VK_F10),
			"F11" => Ok(VirtualKey::VK_F11),
			"F12" => Ok(VirtualKey::VK_F12),
			"F13" => Ok(VirtualKey::VK_F13),
			"F14" => Ok(VirtualKey::VK_F14),
			"F15" => Ok(VirtualKey::VK_F15),
			"F16" => Ok(VirtualKey::VK_F16),
			"F17" => Ok(VirtualKey::VK_F17),
			"F18" => Ok(VirtualKey::VK_F18),
			"F19" => Ok(VirtualKey::VK_F19),
			"F20" => Ok(VirtualKey::VK_F20),
			"F21" => Ok(VirtualKey::VK_F21),
			"F22" => Ok(VirtualKey::VK_F22),
			"F23" => Ok(VirtualKey::VK_F23),
			"F24" => Ok(VirtualKey::VK_F24),
			"NUMLOCK" => Ok(VirtualKey::VK_NUMLOCK),
			"SCROLL" => Ok(VirtualKey::VK_SCROLL),
			"LSHIFT" => Ok(VirtualKey::VK_LSHIFT),
			"RSHIFT" => Ok(VirtualKey::VK_RSHIFT),
			"LCONTROL" => Ok(VirtualKey::VK_LCONTROL),
			"RCONTROL" => Ok(VirtualKey::VK_RCONTROL),
			"LMENU" => Ok(VirtualKey::VK_LMENU),
			"RMENU" => Ok(VirtualKey::VK_RMENU),
			"BROWSER_BACK" => Ok(VirtualKey::VK_BROWSER_BACK),
			"BROWSER_FORWARD" => Ok(VirtualKey::VK_BROWSER_FORWARD),
			"BROWSER_REFRESH" => Ok(VirtualKey::VK_BROWSER_REFRESH),
			"BROWSER_STOP" => Ok(VirtualKey::VK_BROWSER_STOP),
			"BROWSER_SEARCH" => Ok(VirtualKey::VK_BROWSER_SEARCH),
			"BROWSER_FAVORITES" => Ok(VirtualKey::VK_BROWSER_FAVORITES),
			"BROWSER_HOME" => Ok(VirtualKey::VK_BROWSER_HOME),
			"VOLUME_MUTE" => Ok(VirtualKey::VK_VOLUME_MUTE),
			"VOLUME_DOWN" => Ok(VirtualKey::VK_VOLUME_DOWN),
			"VOLUME_UP" => Ok(VirtualKey::VK_VOLUME_UP),
			"MEDIA_NEXT_TRACK" => Ok(VirtualKey::VK_MEDIA_NEXT_TRACK),
			"MEDIA_PREV_TRACK" => Ok(VirtualKey::VK_MEDIA_PREV_TRACK),
			"MEDIA_STOP" => Ok(VirtualKey::VK_MEDIA_STOP),
			"MEDIA_PLAY_PAUSE" => Ok(VirtualKey::VK_MEDIA_PLAY_PAUSE),
			"LAUNCH_MAIL" => Ok(VirtualKey::VK_LAUNCH_MAIL),
			"LAUNCH_MEDIA_SELECT" => Ok(VirtualKey::VK_LAUNCH_MEDIA_SELECT),
			"LAUNCH_APP1" => Ok(VirtualKey::VK_LAUNCH_APP1),
			"LAUNCH_APP2" => Ok(VirtualKey::VK_LAUNCH_APP2),
			"OEM_1" => Ok(VirtualKey::VK_OEM_1),
			"OEM_PLUS" => Ok(VirtualKey::VK_OEM_PLUS),
			"OEM_COMMA" => Ok(VirtualKey::VK_OEM_COMMA),
			"OEM_MINUS" => Ok(VirtualKey::VK_OEM_MINUS),
			"OEM_PERIOD" => Ok(VirtualKey::VK_OEM_PERIOD),
			"OEM_2" => Ok(VirtualKey::VK_OEM_2),
			"OEM_3" => Ok(VirtualKey::VK_OEM_3),
			"OEM_4" => Ok(VirtualKey::VK_OEM_4),
			"OEM_5" => Ok(VirtualKey::VK_OEM_5),
			"OEM_6" => Ok(VirtualKey::VK_OEM_6),
			"OEM_7" => Ok(VirtualKey::VK_OEM_7),
			"OEM_8" => Ok(VirtualKey::VK_OEM_8),
			"OEM_SPECIFIC_0xE1" => Ok(VirtualKey::VK_OEM_SPECIFIC_0xE1),
			"OEM_102" => Ok(VirtualKey::VK_OEM_102),
			"OEM_SPECIFIC_0xE3" => Ok(VirtualKey::VK_OEM_SPECIFIC_0xE3),
			"OEM_SPECIFIC_0xE4" => Ok(VirtualKey::VK_OEM_SPECIFIC_0xE4),
			"PROCESSKEY" => Ok(VirtualKey::VK_PROCESSKEY),
			"OEM_SPECIFIC_0xE6" => Ok(VirtualKey::VK_OEM_SPECIFIC_0xE6),
			"OEM_SPECIFIC_0xE9" => Ok(VirtualKey::VK_OEM_SPECIFIC_0xE9),
			"OEM_SPECIFIC_0xF0" => Ok(VirtualKey::VK_OEM_SPECIFIC_0xF0),
			"OEM_SPECIFIC_0xF1" => Ok(VirtualKey::VK_OEM_SPECIFIC_0xF1),
			"OEM_SPECIFIC_0xF2" => Ok(VirtualKey::VK_OEM_SPECIFIC_0xF2),
			"OEM_SPECIFIC_0xF3" => Ok(VirtualKey::VK_OEM_SPECIFIC_0xF3),
			"OEM_SPECIFIC_0xF4" => Ok(VirtualKey::VK_OEM_SPECIFIC_0xF4),
			"OEM_SPECIFIC_0xF5" => Ok(VirtualKey::VK_OEM_SPECIFIC_0xF5),
			"ATTN" => Ok(VirtualKey::VK_ATTN),
			"CRSEL" => Ok(VirtualKey::VK_CRSEL),
			"EXSEL" => Ok(VirtualKey::VK_EXSEL),
			"EREOF" => Ok(VirtualKey::VK_EREOF),
			"PLAY" => Ok(VirtualKey::VK_PLAY),
			"ZOOM" => Ok(VirtualKey::VK_ZOOM),
			"PA1" => Ok(VirtualKey::VK_PA1),
			"OEM_CLEAR" => Ok(VirtualKey::VK_OEM_CLEAR),
			_ => Err(Error::UnknownName),
		}
	}
}

impl fmt::Display for VirtualKey {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		use VirtualKey::*;
		write!(
			f,
			"{}",
			match &self {
				VK_BACK => "BACK",
				VK_TAB => "TAB",
				VK_CLEAR => "CLEAR",
				VK_RETURN => "RETURN",
				VK_SHIFT => "SHIFT",
				VK_CONTROL => "CONTROL",
				VK_MENU => "MENU",
				VK_PAUSE => "PAUSE",
				VK_CAPITAL => "CAPITAL",
				VK_IME_15 => "IME_15",
				VK_IME_17 => "IME_17",
				VK_IME_18 => "IME_18",
				VK_IME_19 => "IME_19",
				VK_ESCAPE => "ESCAPE",
				VK_ACCEPT => "ACCEPT",
				VK_SPACE => "SPACE",
				VK_PRIOR => "PRIOR",
				VK_NEXT => "NEXT",
				VK_END => "END",
				VK_HOME => "HOME",
				VK_LEFT => "LEFT",
				VK_UP => "UP",
				VK_RIGHT => "RIGHT",
				VK_DOWN => "DOWN",
				VK_SELECT => "SELECT",
				VK_PRINT => "PRINT",
				VK_EXECUTE => "EXECUTE",
				VK_SNAPSHOT => "SNAPSHOT",
				VK_INSERT => "INSERT",
				VK_DELETE => "DELETE",
				VK_HELP => "HELP",
				VK_ZERO => "ZERO",
				VK_ONE => "ONE",
				VK_TWO => "TWO",
				VK_THREE => "THREE",
				VK_FOUR => "FOUR",
				VK_FIVE => "FIVE",
				VK_SIX => "SIX",
				VK_SEVEN => "SEVEN",
				VK_EIGHT => "EIGHT",
				VK_NINE => "NINE",
				VK_A => "A",
				VK_B => "B",
				VK_C => "C",
				VK_D => "D",
				VK_E => "E",
				VK_F => "F",
				VK_G => "G",
				VK_H => "H",
				VK_I => "I",
				VK_J => "J",
				VK_K => "K",
				VK_L => "L",
				VK_M => "M",
				VK_N => "N",
				VK_O => "O",
				VK_P => "P",
				VK_Q => "Q",
				VK_R => "R",
				VK_S => "S",
				VK_T => "T",
				VK_U => "U",
				VK_V => "V",
				VK_W => "W",
				VK_X => "X",
				VK_Y => "Y",
				VK_Z => "Z",
				VK_LWIN => "LWIN",
				VK_RWIN => "RWIN",
				VK_APPS => "APPS",
				VK_SLEEP => "SLEEP",
				VK_NUMPAD0 => "NUMPAD0",
				VK_NUMPAD1 => "NUMPAD1",
				VK_NUMPAD2 => "NUMPAD2",
				VK_NUMPAD3 => "NUMPAD3",
				VK_NUMPAD4 => "NUMPAD4",
				VK_NUMPAD5 => "NUMPAD5",
				VK_NUMPAD6 => "NUMPAD6",
				VK_NUMPAD7 => "NUMPAD7",
				VK_NUMPAD8 => "NUMPAD8",
				VK_NUMPAD9 => "NUMPAD9",
				VK_MULTIPLY => "MULTIPLY",
				VK_ADD => "ADD",
				VK_SEPARATOR => "SEPARATOR",
				VK_SUBSTRACT => "SUBSTRACT",
				VK_DECIMAL => "DECIMAL",
				VK_DIVIDE => "DIVIDE",
				VK_F1 => "F1",
				VK_F2 => "F2",
				VK_F3 => "F3",
				VK_F4 => "F4",
				VK_F5 => "F5",
				VK_F6 => "F6",
				VK_F7 => "F7",
				VK_F8 => "F8",
				VK_F9 => "F9",
				VK_F10 => "F10",
				VK_F11 => "F11",
				VK_F12 => "F12",
				VK_F13 => "F13",
				VK_F14 => "F14",
				VK_F15 => "F15",
				VK_F16 => "F16",
				VK_F17 => "F17",
				VK_F18 => "F18",
				VK_F19 => "F19",
				VK_F20 => "F20",
				VK_F21 => "F21",
				VK_F22 => "F22",
				VK_F23 => "F23",
				VK_F24 => "F24",
				VK_NUMLOCK => "NUMLOCK",
				VK_SCROLL => "SCROLL",
				VK_LSHIFT => "LSHIFT",
				VK_RSHIFT => "RSHIFT",
				VK_LCONTROL => "LCONTROL",
				VK_RCONTROL => "RCONTROL",
				VK_LMENU => "LMENU",
				VK_RMENU => "RMENU",
				VK_BROWSER_BACK => "BROWSER_BACK",
				VK_BROWSER_FORWARD => "BROWSER_FORWARD",
				VK_BROWSER_REFRESH => "BROWSER_REFRESH",
				VK_BROWSER_STOP => "BROWSER_STOP",
				VK_BROWSER_SEARCH => "BROWSER_SEARCH",
				VK_BROWSER_FAVORITES => "BROWSER_FAVORITES",
				VK_BROWSER_HOME => "BROWSER_HOME",
				VK_VOLUME_MUTE => "VOLUME_MUTE",
				VK_VOLUME_DOWN => "VOLUME_DOWN",
				VK_VOLUME_UP => "VOLUME_UP",
				VK_MEDIA_NEXT_TRACK => "MEDIA_NEXT_TRACK",
				VK_MEDIA_PREV_TRACK => "MEDIA_PREV_TRACK",
				VK_MEDIA_STOP => "MEDIA_STOP",
				VK_MEDIA_PLAY_PAUSE => "MEDIA_PLAY_PAUSE",
				VK_LAUNCH_MAIL => "LAUNCH_MAIL",
				VK_LAUNCH_MEDIA_SELECT => "LAUNCH_MEDIA_SELECT",
				VK_LAUNCH_APP1 => "LAUNCH_APP1",
				VK_LAUNCH_APP2 => "LAUNCH_APP2",
				VK_OEM_1 => "OEM_1",
				VK_OEM_PLUS => "OEM_PLUS",
				VK_OEM_COMMA => "OEM_COMMA",
				VK_OEM_MINUS => "OEM_MINUS",
				VK_OEM_PERIOD => "OEM_PERIOD",
				VK_OEM_2 => "OEM_2",
				VK_OEM_3 => "OEM_3",
				VK_OEM_4 => "OEM_4",
				VK_OEM_5 => "OEM_5",
				VK_OEM_6 => "OEM_6",
				VK_OEM_7 => "OEM_7",
				VK_OEM_8 => "OEM_8",
				VK_OEM_SPECIFIC_0xE1 => "OEM_SPECIFIC_0xE1",
				VK_OEM_102 => "OEM_102",
				VK_OEM_SPECIFIC_0xE3 => "OEM_SPECIFIC_0xE3",
				VK_OEM_SPECIFIC_0xE4 => "OEM_SPECIFIC_0xE4",
				VK_PROCESSKEY => "PROCESSKEY",
				VK_OEM_SPECIFIC_0xE6 => "OEM_SPECIFIC_0xE6",
				VK_OEM_SPECIFIC_0xE9 => "OEM_SPECIFIC_0xE9",
				VK_OEM_SPECIFIC_0xF0 => "OEM_SPECIFIC_0xF0",
				VK_OEM_SPECIFIC_0xF1 => "OEM_SPECIFIC_0xF1",
				VK_OEM_SPECIFIC_0xF2 => "OEM_SPECIFIC_0xF2",
				VK_OEM_SPECIFIC_0xF3 => "OEM_SPECIFIC_0xF3",
				VK_OEM_SPECIFIC_0xF4 => "OEM_SPECIFIC_0xF4",
				VK_OEM_SPECIFIC_0xF5 => "OEM_SPECIFIC_0xF5",
				VK_ATTN => "ATTN",
				VK_CRSEL => "CRSEL",
				VK_EXSEL => "EXSEL",
				VK_EREOF => "EREOF",
				VK_PLAY => "PLAY",
				VK_ZOOM => "ZOOM",
				VK_PA1 => "PA1",
				VK_OEM_CLEAR => "OEM_CLEAR",
			}
		)
	}
}

impl ser::Serialize for VirtualKey {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: ser::Serializer,
	{
		serializer.serialize_str(&format!("{}", self))
	}
}

impl<'de> de::Deserialize<'de> for VirtualKey {
	fn deserialize<D>(deserializer: D) -> Result<VirtualKey, D::Error>
	where
		D: de::Deserializer<'de>,
	{
		struct VirtualKeyVisitor;

		impl<'de> de::Visitor<'de> for VirtualKeyVisitor {
			type Value = VirtualKey;

			fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
				formatter.write_str("a valid VirtualKey descriptor")
			}

			fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
			where
				E: de::Error,
			{
				match VirtualKey::parse(s) {
					Ok(v) => Ok(v),
					Err(_) => Err(E::custom("unknown VirtualKey")),
				}
			}
		}

		deserializer.deserialize_str(VirtualKeyVisitor)
	}
}

#[derive(Debug)]
pub enum Error {
	Reserved,
	UnknownByte,
	UnknownName,
}
