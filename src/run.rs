use crate::{
	config::{Controller, Note},
	keys::{send_key_down, send_key_up, VirtualKey},
	midi::{MidiEvent, MidiMessage},
	utils::clamp2,
};
use crossbeam_channel::{Receiver, TryRecvError};
use std::{
	collections::HashMap,
	thread,
	time::{Duration, Instant},
};

#[derive(Debug, Hash, Eq, PartialEq)]
struct KeyState {
	pub down:     Option<Instant>,
	pub down_for: Option<Duration>,
}

type State = HashMap<crate::keys::VirtualKey, KeyState, BSeaHash>;

struct BSeaHash;
impl std::hash::BuildHasher for BSeaHash {
	type Hasher = seahash::SeaHasher;
	fn build_hasher(&self) -> Self::Hasher { seahash::SeaHasher::default() }
}

#[derive(Debug)]
pub struct RunLoop<'t> {
	state:         State,
	incoming:      Receiver<MidiMessage>,
	loop_interval: Duration,

	map_notes:       &'t [Note],
	map_controllers: &'t [Controller],
}

impl<'t> RunLoop<'t> {
	pub fn new(
		state_cap: usize,
		incoming: Receiver<MidiMessage>,
		mn: &'t [Note],
		mp: &'t [Controller],
	) -> RunLoop<'t> {
		Self {
			state: HashMap::with_capacity_and_hasher(state_cap, BSeaHash),
			incoming,
			loop_interval: Duration::from_millis(5),

			map_notes: mn,
			map_controllers: mp,
		}
	}

	#[allow(dead_code)]
	pub fn set_loop_interval(&mut self, interval: Duration) { self.loop_interval = interval; }

	pub fn run_forever(mut self) {
		loop {
			if !self.handle_events() {
				// channel has been disconnected, stop doing anything
				return;
			};
			self.handle_timeouts();
			thread::sleep(self.loop_interval);
		}
	}

	/// Handle all queued events
	#[inline]
	fn handle_events(&mut self) -> bool {
		loop {
			let event = match self.incoming.try_recv() {
				Ok(ev) => ev,
				Err(err) => match err {
					// break with "false" when the producer thread is not there anymore
					TryRecvError::Disconnected => break false,
					// break with "true" when we just emptied the queue
					TryRecvError::Empty => break true,
				},
			};

			match event {
				MidiMessage::Note(nevent) => {
					for mp in self.map_notes {
						if nevent.event == MidiEvent::NoteOn
							&& mp.note == nevent.note && nevent.velocity >= mp.min_vel
							&& nevent.velocity <= mp.max_vel
							&& nevent.channel == mp.channel
						{
							self.set_key(mp.keycode, mp.ara, true);
						} else if nevent.event == MidiEvent::NoteOff
							&& mp.note == nevent.note && nevent.channel == mp.channel
						{
							self.set_key(mp.keycode, mp.ara, false);
						};
					}
				},
				MidiMessage::Controller(cevent) => {
					for ct in self.map_controllers {
						if ct.controller == cevent.controller {
							let upper = clamp2(0, 127, ct.midpoint.saturating_add(ct.deadzone));
							let lower = clamp2(0, 127, ct.midpoint.saturating_sub(ct.deadzone));

							if cevent.value >= upper {
								self.set_key(ct.keycode, ct.ara, true);
							} else if cevent.value <= lower {
								self.set_key(ct.keycode, ct.ara, false);
							}
						}
					}
				},
			}
		}
	}

	/// Process all the upcoming timeouts
	#[inline]
	fn handle_timeouts(&mut self) {
		let now = Instant::now();
		for (k, s) in self
			.state
			.iter_mut()
			.filter(|(_, v)| v.down.is_some() && v.down_for.is_some())
			.filter(|(_, KeyState { down, down_for })| {
				now.duration_since(down.unwrap()) >= down_for.unwrap()
			}) {
			log::debug!("key timed out {:?}[{:?}->{:?}]", k, s.down, s.down_for);
			match send_key_up(*k) {
				Ok(_) => {
					s.down = None;
					s.down_for = None;
				},
				Err(e) => {
					log::warn!("keyup failed: {:?}", e);
				},
			}
		}
	}

	#[inline]
	fn set_key(&mut self, vk: VirtualKey, ara: Option<u64>, down: bool) {
		log::debug!("set key {:?}[{:?}]->{}", vk, ara, down);
		let state = self.state.entry(vk).or_insert_with(|| KeyState {
			down:     None,
			down_for: None,
		});
		log::trace!("state = {:?}", state);
		if down {
			match send_key_down(vk) {
				Ok(_) => {
					state.down = Some(Instant::now());
					if let Some(ara) = ara {
						state.down_for = Some(Duration::from_millis(ara))
					}
				},
				Err(e) => {
					log::warn!("keydown failed: {:?}", e);
				},
			}
		} else {
			match send_key_up(vk) {
				Ok(_) => {
					state.down = None;
					state.down_for = None;
				},
				Err(e) => {
					log::warn!("keyup failed: {:?}", e);
				},
			}
		}
	}
}
