# Keys

List of keys as they can be specified in the config. Case does not matter. alternatives separated with `,`

| Key                 | Notes                                                               |
| ------------------- | ------------------------------------------------------------------- |
| BACK                |                                                                     |
| TAB                 |                                                                     |
| CLEAR               |                                                                     |
| RETURN              |                                                                     |
| SHIFT               |                                                                     |
| CONTROL             |                                                                     |
| MENU                | aka ALT                                                             |
| PAUSE               |                                                                     |
| CAPITAL             |                                                                     |
| IME_15              | Special IME (International) Key: Kana, Hangul                       |
| IME_17              | Special IME (International) Key: Junja                              |
| IME_18              | Special IME (International) Key: FINAL (used to finalize IME input) |
| IME_19              | Special IME (International) Key: Hanja, Kanji                       |
| ESCAPE              |                                                                     |
| ACCEPT              |                                                                     |
| SPACE               |                                                                     |
| PRIOR               |                                                                     |
| NEXT                |                                                                     |
| END                 |                                                                     |
| HOME                |                                                                     |
| LEFT                |                                                                     |
| UP                  |                                                                     |
| RIGHT               |                                                                     |
| DOWN                |                                                                     |
| SELECT              |                                                                     |
| PRINT               |                                                                     |
| EXECUTE             |                                                                     |
| SNAPSHOT            | aka PrintScr                                                        |
| INSERT              |                                                                     |
| DELETE              |                                                                     |
| HELP                |                                                                     |
| ZERO, 0             |                                                                     |
| ONE, 1              |                                                                     |
| TWO, 2              |                                                                     |
| THREE, 3            |                                                                     |
| FOUR, 4             |                                                                     |
| FIVE, 5             |                                                                     |
| SIX, 6              |                                                                     |
| SEVEN, 7            |                                                                     |
| EIGHT, 8            |                                                                     |
| NINE, 9             |                                                                     |
| A                   |                                                                     |
| B                   |                                                                     |
| C                   |                                                                     |
| D                   |                                                                     |
| E                   |                                                                     |
| F                   |                                                                     |
| G                   |                                                                     |
| H                   |                                                                     |
| I                   |                                                                     |
| J                   |                                                                     |
| K                   |                                                                     |
| L                   |                                                                     |
| M                   |                                                                     |
| N                   |                                                                     |
| O                   |                                                                     |
| P                   |                                                                     |
| Q                   |                                                                     |
| R                   |                                                                     |
| S                   |                                                                     |
| T                   |                                                                     |
| U                   |                                                                     |
| V                   |                                                                     |
| W                   |                                                                     |
| X                   |                                                                     |
| Y                   |                                                                     |
| Z                   |                                                                     |
| LWIN                |                                                                     |
| RWIN                |                                                                     |
| APPS                |                                                                     |
| SLEEP               |                                                                     |
| NUMPAD0             |                                                                     |
| NUMPAD1             |                                                                     |
| NUMPAD2             |                                                                     |
| NUMPAD3             |                                                                     |
| NUMPAD4             |                                                                     |
| NUMPAD5             |                                                                     |
| NUMPAD6             |                                                                     |
| NUMPAD7             |                                                                     |
| NUMPAD8             |                                                                     |
| NUMPAD9             |                                                                     |
| MULTIPLY            |                                                                     |
| ADD                 |                                                                     |
| SEPARATOR           |                                                                     |
| SUBSTRACT           |                                                                     |
| DECIMAL             |                                                                     |
| DIVIDE              |                                                                     |
| F1                  |                                                                     |
| F2                  |                                                                     |
| F3                  |                                                                     |
| F4                  |                                                                     |
| F5                  |                                                                     |
| F6                  |                                                                     |
| F7                  |                                                                     |
| F8                  |                                                                     |
| F9                  |                                                                     |
| F10                 |                                                                     |
| F11                 |                                                                     |
| F12                 |                                                                     |
| F13                 |                                                                     |
| F14                 |                                                                     |
| F15                 |                                                                     |
| F16                 |                                                                     |
| F17                 |                                                                     |
| F18                 |                                                                     |
| F19                 |                                                                     |
| F20                 |                                                                     |
| F21                 |                                                                     |
| F22                 |                                                                     |
| F23                 |                                                                     |
| F24                 |                                                                     |
| NUMLOCK             |                                                                     |
| SCROLL              |                                                                     |
| LSHIFT              | usually the same as `SHIFT`                                         |
| RSHIFT              |                                                                     |
| LCONTROL            | usually the same as `CONTROL`                                       |
| RCONTROL            |                                                                     |
| LMENU               | usually the same as `MENU`, aka ALT                                 |
| RMENU               | aka ALT                                                             |
| BROWSER_BACK        |                                                                     |
| BROWSER_FORWARD     |                                                                     |
| BROWSER_REFRESH     |                                                                     |
| BROWSER_STOP        |                                                                     |
| BROWSER_SEARCH      |                                                                     |
| BROWSER_FAVORITES   |                                                                     |
| BROWSER_HOME        |                                                                     |
| VOLUME_MUTE         |                                                                     |
| VOLUME_DOWN         |                                                                     |
| VOLUME_UP           |                                                                     |
| MEDIA_NEXT_TRACK    |                                                                     |
| MEDIA_PREV_TRACK    |                                                                     |
| MEDIA_STOP          |                                                                     |
| MEDIA_PLAY_PAUSE    |                                                                     |
| LAUNCH_MAIL         |                                                                     |
| LAUNCH_MEDIA_SELECT |                                                                     |
| LAUNCH_APP1         | launched app depends on the context this key is pressed in          |
| LAUNCH_APP2         | launched app depends on the context this key is pressed in          |
| OEM_PLUS            | for any country/region, the `+` key                                 |
| OEM_COMMA           | for any country/region, the `,` key                                 |
| OEM_MINUS           | for any country/region, the `-` key                                 |
| OEM_PERIOD          | for any country/region, the `.` key                                 |
| PLAY                |                                                                     |
| ZOOM                |                                                                     |
