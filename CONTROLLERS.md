# Controller & Notes

Case does not matter.

## Controller

| Name                                         | Controller           |
| -------------------------------------------- | -------------------- |
| modulation                                   | Modulation           |
| breath                                       | Breath               |
| foot_controller, footcontroller              | FootController       |
| portamento_time, portamentotime              | PortamentoTime       |
| volume                                       | Volume               |
| balance                                      | Balance              |
| pan                                          | Pan                  |
| expression                                   | Expression           |
| pedal_sustain, pedalsustain                  | PedalSustain         |
| portamento                                   | Portamento           |
| pedal_sostenuto, pedalsostenuto              | PedalSostenuto       |
| pedal_soft, pedalsoft                        | PedalSoft            |
| hold2                                        | Hold2                |
| external_effects_depth, externaleffectsdepth | ExternalEffectsDepth |
| tremolo_depth, tremolodepth                  | TremoloDepth         |
| chorus_depth, chorusdepth                    | ChorusDepth          |
| celeste_detune_depth, celestedetunedepth     | CelesteDetuneDepth   |
| phaser_depth, phaserdepth                    | PhaserDepth          |

## Notes

| Name | Note |
| ---- | ---- |
| cn   | Cn   |
| csn  | Csn  |
| dn   | Dn   |
| dsn  | Dsn  |
| en   | En   |
| fn   | Fn   |
| fsn  | Fsn  |
| gn   | Gn   |
| gsn  | Gsn  |
| an   | An   |
| asn  | Asn  |
| bn   | Bn   |
| c0   | C0   |
| cs0  | Cs0  |
| d0   | D0   |
| ds0  | Ds0  |
| e0   | E0   |
| f0   | F0   |
| fs0  | Fs0  |
| g0   | G0   |
| gs0  | Gs0  |
| a0   | A0   |
| as0  | As0  |
| b0   | B0   |
| c1   | C1   |
| cs1  | Cs1  |
| d1   | D1   |
| ds1  | Ds1  |
| e1   | E1   |
| f1   | F1   |
| fs1  | Fs1  |
| g1   | G1   |
| gs1  | Gs1  |
| a1   | A1   |
| as1  | As1  |
| b1   | B1   |
| c2   | C2   |
| cs2  | Cs2  |
| d2   | D2   |
| ds2  | Ds2  |
| e2   | E2   |
| f2   | F2   |
| fs2  | Fs2  |
| g2   | G2   |
| gs2  | Gs2  |
| a2   | A2   |
| as2  | As2  |
| b2   | B2   |
| c3   | C3   |
| cs3  | Cs3  |
| d3   | D3   |
| ds3  | Ds3  |
| e3   | E3   |
| f3   | F3   |
| fs3  | Fs3  |
| g3   | G3   |
| gs3  | Gs3  |
| a3   | A3   |
| as3  | As3  |
| b3   | B3   |
| c4   | C4   |
| cs4  | Cs4  |
| d4   | D4   |
| ds4  | Ds4  |
| e4   | E4   |
| f4   | F4   |
| fs4  | Fs4  |
| g4   | G4   |
| gs4  | Gs4  |
| a4   | A4   |
| as4  | As4  |
| b4   | B4   |
| c5   | C5   |
| cs5  | Cs5  |
| d5   | D5   |
| ds5  | Ds5  |
| e5   | E5   |
| f5   | F5   |
| fs5  | Fs5  |
| g5   | G5   |
| gs5  | Gs5  |
| a5   | A5   |
| as5  | As5  |
| b5   | B5   |
| c6   | C6   |
| cs6  | Cs6  |
| d6   | D6   |
| ds6  | Ds6  |
| e6   | E6   |
| f6   | F6   |
| fs6  | Fs6  |
| g6   | G6   |
| gs6  | Gs6  |
| a6   | A6   |
| as6  | As6  |
| b6   | B6   |
| c7   | C7   |
| cs7  | Cs7  |
| d7   | D7   |
| ds7  | Ds7  |
| e7   | E7   |
| f7   | F7   |
| fs7  | Fs7  |
| g7   | G7   |
| gs7  | Gs7  |
| a7   | A7   |
| as7  | As7  |
| b7   | B7   |
| c8   | C8   |
| cs8  | Cs8  |
| d8   | D8   |
| ds8  | Ds8  |
| e8   | E8   |
| f8   | F8   |
| fs8  | Fs8  |
| g8   | G8   |
| gs8  | Gs8  |
| a8   | A8   |
| as8  | As8  |
| b8   | B8   |
| c9   | C9   |
| cs9  | Cs9  |
| d9   | D9   |
| ds9  | Ds9  |
| e9   | E9   |
| f9   | F9   |
| fs9  | Fs9  |
| g9   | G9   |
